---
title: "Assurance IQ"
description: "Receive a confidence level for a user’s digital identity to determine if a further form of authentication is suggested."
date: 2018-08-14T15:16:16+08:00
draft: false
tags: ["issuer", "merchant", "security"]
pageTitle: "Asuurance IQ Product Name"
supportEmail: "assurance_support@mastercard.com"
supportPhone: "+6588585585"
---

# Assurance IQ product landing page.

{{< flex >}}

{{< flex-column >}}
  <center>
    <p>this is your first column!</p>
  </center>
{{< /flex-column >}}

{{< flex-column >}}
  <center>
    <p>this is your second column!</p>
  </center>
{{< /flex-column >}}

{{< flex-column >}}
  <p>this is your third column!</p>
{{< /flex-column >}}

{{< flex-column >}}

## Testing

  * Testing
  * Testing 2
  * Testing 3

{{< /flex-column >}}

{{< /flex >}}