---
title: "Money Send"
description: "Move funds quickly and safely between MasterCard card accounts and send disbursements leveraging the trusted and reliable MasterCard Network."
date: 2018-08-14T15:16:16+08:00
draft: false
tags: ["issuer", "merchant", "payments"]
url:
---

# Money Send product landing page.