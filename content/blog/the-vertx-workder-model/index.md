---
title: "The Vert.x Worker Model"
date: 2018-08-14T15:16:16+08:00
draft: false
layout: blog
author: "Sébastien Le Callonnec"
resources:
- name: "heroImage"
  src: "hero.png"
---

Payment gateways such as the Mastercard Payment Gateway process hundreds of millions of payments every day. Building and maintaining complex platforms such as gateways requires a team of engineers to study traffic patterns and ensure stability.
Our white label small business platform is a reactive system designed to be highly available: a reactive platform makes our platform responsive, resilient, elastic and message-driven. To build this platform, we have used Vert.x to call the various services required to process a payment – all in an asynchronous fashion.

## Reactive Systems and Vert.x
Reactive systems have emerged in response to the increasing need of highly-available elastic applications which have shown the limits of classic threaded systems. Those systems show resilience when faced with failure and favor loose coupling through asynchronous message-passing. More generally speaking, reactive has become synonymous with working with non-blocking streams of data.
Vert.x is a JVM toolkit that can be used to build reactive systems. It is event-driven and uses message passing for processing work; those messages are by default handled by the event loop which is (usually) one thread responsible to find the right handler for an incoming message, and process it. As a result, the “golden rule” is: never block the event loop!
But what if you need to process long-running tasks? Those long tasks will prevent any other task from being performed until they are complete, resulting in severe performance degradations, or even catastrophic failures. To solve that problem, Vert.x introduces the concept of worker verticles.
A Vert.x application is typically composed of one or several verticles, which are similar to modules, executing in their own context on top of a Vert.x instance. Worker verticles are special ones that can be used to process tasks that would block the event loop, and the purpose of this post is to look at them and see how they fit in reactive systems built with Vert.x.
All code for this article can be found on the Payment Gateway Github.

## Let's Get Reactive Coffee!
*Blocking the event loop*
Following the long-standing tradition of using a coffee shop, we are going to use a simple API that allows customers to order different types of coffee. The coffee is then prepared by a barista in the background, in an asynchronous fashion.
Our application has two simple verticles:
A HTTP server verticle, that receives the incoming orders, logs them and sends them to the event bus for processing
A verticle that picks up the message from the event bus and processes it. A long-running task is simulated by a Thread.sleep for a random length of time

![Example image](vert-diagram01-min.png)

