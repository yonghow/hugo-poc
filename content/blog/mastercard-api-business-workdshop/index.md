---
title: "Mastercard API Business Workshop"
date: 2018-08-14T15:16:16+08:00
draft: false
layout: blog
HeroImage: "hero.png"
author: "Ed"
resources:
- name: "heroImage"
  src: "hero.png"
---
This blog follows on from Oran’s Applying an API Lens blog where he talks about the critical benefits of applying an API lens to your business and not just thinking of an API as a technical imperative. Here in Mastercard, we have created a program called the ‘API Business Workshop’ to help product teams undertake this assessment and this is what I’ll cover in this latest blog.

## Why is this important now?
Today, there is an accelerated shift towards the use of APIs. This is mostly due to the increasing necessity to partner to deliver the optimum solution at scale. Also, regulatory changes (like PSD2) are further accelerating this shift for us in the financial services industry. This evolution represents a huge opportunity as APIs are opening up new business opportunities and driving revenue growth through:
* Addressing underserved segments
* Realizing new growth opportunities through increased distribution
* Extending product capabilities
* Improving operational efficiencies
* Unlocking new business models
It’s in that context that we created the ‘API Business Workshop’. The objective here is to apply an ‘API lens’ to our existing businesses and assets to surface and maximize commercial potential through APIs.

What happens during an API Workshop?
The most important part of any workshop is having the right people present; product folk, subject matter experts, technology folk to ensure there’s a 360 degree perspective on any idea.
The format is broken into 3 sections:
1. All about APIs
We seed the conversation by looking at Mastercard examples and other companies who have built successful business through adopting an API-first philosophy, key learning’s and insights being highlighted. Nothing technical here, it’s all about how the business has evolved through an API focus.
Given that much of the audience is non-technical, we do have a section on ‘demystifying APIs’ where we walk through our Mastercard Developers platform explaining what APIs are and how customers and partners typically gain access to our services through APIs.
2. All about your product
The engagement flips over to the business area of focus. Here, the product and business experts take the floor to lay out vision, goals, product capabilities, customers, roadmap and strategy also highlighting key risks, competitors and industry trends. The objective here is to fuel the 3rd section which is all about ideating on how APIs could help accelerate goals or even tweak the vision!
3. Exploring API opportunities for you
This is the fun bit. We take to the whiteboard and starting ideating on how APIs can help. The focus is very much commercial in nature – every idea is discussed in terms of enhanced value proposition, increased distribution, unlocking new types of customers and new monetization models. Filtering using techniques such as RWW (Is it Real? Can we Win? Is it Worth it?) are very useful to ensure we don’t get distracted by something that might be cool but not necessarily best for the business.
We started running these workshops within Mastercard to singularly focus on individual areas of the business but what has become quite obvious to us is that there’s lots of potential to surface opportunity through cross pollination from multiple business areas; how products can be bundled with other to create brand new value propositions.
We will continue to evolve this program and post some of the learning’s output and case studies in future blogs.