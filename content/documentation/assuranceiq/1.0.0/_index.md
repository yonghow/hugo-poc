---
title: Documentation - Assurance IQ
date: 2018-08-14 07:16:16 +0000
layout: documentation
---
# Assurance IQ

<a name="mdesoverview"></a>

## Overview

Lost Stolen fraud service

<a name="mdesdefinitions"></a>

## Definitions

<a name="mdesaccount"></a>

### Account

| Name | Description | Schema |
| --- | --- | --- |
| Listed  <br>required | Indicates if the account is listed on the Lost-Stolen Account List. true = Yes, false = No  <br>Example : true | boolean |
| Reason  <br>optional | Textual description of ReasonCode.  <br>Example : "STOLEN" | string |
| ReasonCode  <br>optional | F - FRAUD<br>L - LOST<br>P - CAPTURE CARD OR PICKUP<br>S - STOLEN<br>U - UNAUTHORIZED USE<br>X - COUNTERFEIT  <br>Example : "S" | enum (F, L, P, S, U, X) |
| Status  <br>required | Indicates if the call (query) was successful or if there were errors in processing.  <br>Example : true | boolean |

#### Kittens

#### Get All Kittens
{{< code-snippet filePath="documentation/assuranceiq/1.0.0/sample-code/get-request.md" >}}

#### Post All Kittens
{{< code-snippet filePath="documentation/assuranceiq/1.0.0/sample-code/post-request.md" >}}

<a name="mdesaccountinquiry"></a>

### AccountInquiry

| Name | Description | Schema |
| --- | --- | --- |
| AccountNumber  <br>optional | The card number which is used to validate against the Lost-Stolen Account List.  <br>Length : 12 - 19  <br>Example : "5343434343434343" | string |

<a name="mdesaccountinquiryrequest"></a>

### AccountInquiryRequest

| Name | Description | Schema |
| --- | --- | --- |
| AccountInquiry  <br>optional | Example : "\[mdesaccountinquiry\](#mdesaccountinquiry)" | AccountInquiry |

<a name="mdesaccountinquiryresponse"></a>

### AccountInquiryResponse

| Name | Description | Schema |
| --- | --- | --- |
| Account  <br>optional | Example : "\[mdesaccount\](#mdesaccount)" | Account |

<a name="mdeserrormodel"></a>

### errorModel

| Name | Description | Schema |
| --- | --- | --- |
| code  <br>required | Example : 0 | integer (int32) |
| message  <br>required | Example : "string" | string |

<a name="mdespaths"></a>

## Paths

<a name="mdes-account-inquiry-put"></a>

### Validate Account

Provides the ability to validate accounts against a list of MasterCard accounts that have been reported by issuers to be lost or stolen.

    PUT /account-inquiry

#### Parameters

| Type | Name | Schema |
| --- | --- | --- |
| Body | AccountInquiryRequest  <br>required | AccountInquiryRequest |

#### Responses

| HTTP Code | Description | Schema |
| --- | --- | --- |
| 200 | Account inquiry response | AccountInquiryResponse |
| default | unexpected error | errorModel |

#### Tags

* Account Inquiry

#### Example HTTP request

##### Request path

    /account-inquiry

##### Request body

```json
{
  "AccountInquiry" : {
    "AccountNumber" : "5343434343434343"
  }
}
```

#### Example HTTP response

##### Response 200

>     {
>       "Account" : {
>         "Status" : true,
>         "Listed" : true,
>         "ReasonCode" : "S",
>         "Reason" : "STOLEN"
>       }
>     }

##### Response default

    {
    	"code" : 0,
        "message" : "string"
    }

Testing

## FAQ
{{< get-markdown "faq.md" >}}