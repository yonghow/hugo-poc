---
title: "Sample Code - Get Request"
date: 2018-08-14T15:16:16+08:00
draft: false
---

{{< highlight go >}}
	package main

	import "github.com/bep/kittn/auth"

	func main() {
		api := auth.Authorize("meowmeowmeow")

		_ = api.GetKittens()
	}
{{< /highlight >}}

{{< highlight ruby >}}
	require 'kittn'

	api = Kittn::APIClient.authorize!('meowmeowmeow')
	api.kittens.get
{{< /highlight >}}

{{< highlight python >}}
	import kittn

	api = kittn.authorize('meowmeowmeow')
	api.kittens.get()
{{< /highlight >}}

{{< highlight shell >}}
	curl "http://example.com/api/kittens"
		-H "Authorization: meowmeowmeow"
{{< /highlight >}}

{{< highlight javascript >}}
	const kittn = require('kittn');

	let api = kittn.authorize('meowmeowmeow');
	let kittens = api.kittens.get();
{{< /highlight >}}
