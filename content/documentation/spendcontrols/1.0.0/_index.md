---
title: "Masterpass"
date: 2018-08-14T15:16:16+08:00
draft: false
---

# Spend Controls Documentation


<a name="mdesoverview"></a>
## Overview
MasterCard Spend Controls API


### Version information
*Version* : 1.0


### Contact information
*Contact* : Mastercard
*Contact Email* : mastercard@mastercard.com


### License information
*License* : Mastercard
*Terms of service* : null


### URI scheme
*Host* : api.mastercard.com
*BasePath* : /issuer/spendcontrols/v1


### Tags

* Card : Card
* alert-all : Alert All
* alerts : Alerts
* amount : Amount
* amount-decline : Amount Decline
* batch : Batch
* budget-alert : Budget alert
* budget-alert-with-budgetId : Budget alert with budget Id
* budget-decline : Budget decline
* budget-decline-with-budgetId : Budget decline with budget Id
* channel : Channel
* channel-decline : Channel Decline
* combination-ctrls-alert-resource : Combination Ctrls Alert Resource
* combination-ctrls-alert-resource-filterId : Combination Ctrls Alert Resource By FilterId
* combination-ctrls-decline-resource : Combination Ctrls Decline Resource
* combination-ctrls-decline-resource-filterId : Combination Ctrls Decline Resource By FilterId
* controls : Controls
* controls-history : Controls History
* cross-border : Cross Border
* cross-border-decline : Cross Border Decline
* decline-all : Decline All
* declines : Declines
* geolocation-alert : Geolocation Alert
* geolocation-decline : Geolocation Decline
* merchant-category-code-alert : Merchant Category Code Alert
* merchant-category-code-decline : Merchant Category Code Decline
* notification : Notification
* travel-alert : Travel alert
* travel-alert-with-travelId : Travel alert with travel Id
* travel-decline : Travel decline
* travel-decline-with-travelId : Travel decline with travel Id
* txn-history : Txn History
* verify : Verify


<a name="mdespaths"></a>
## Paths

<a name="mdesregistercardusingpost"></a>
### Register a user card.
```
POST /card
```


#### Description
Creates a new card and assign a UUID for the card to be used in subsequent operations.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**accountNumber**  <br>*required*|Card number (Primary Account Number).|[PanDTO](#mdespandto)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[EncodedPanDTO](#mdesencodedpandto)|
|**400**|Invalid Account Number.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|Possibe causes:<br>ClientID not provisioned.<br>Account Number not allowed to be provisioned for the registered ICA associated with ClientID.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* Card


#### Example HTTP request

##### Request path
```
/card
```


##### Request body
```json
{
  "accountNumber" : "5204736610784748"
}
```


#### Example HTTP response

##### Response 200
```json
{
  "uuid" : "5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg"
}
```


<a name="mdesretrieveuuidusingpost"></a>
### Retrieves UUID for a given PAN
```
POST /card/uuid
```


#### Description
Retrieves the UUID for a registered PAN.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**body**  <br>*required*|Card number (Primary Account Number).|[PanDTO](#mdespandto)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[EncodedPanDTO](#mdesencodedpandto)|
|**400**|Invalid Account Number.|No Content|
|**401**|Possibe causes:<br>ClientID not provisioned.<br>Account Number not allowed to be provisioned for the registered ICA associated with ClientID.|No Content|
|**404**|The card is not provisioned.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* Card


#### Example HTTP request

##### Request path
```
/card/uuid
```


##### Request body
```json
{
  "accountNumber" : "5204736610784748"
}
```


#### Example HTTP response

##### Response 200
```json
{
  "uuid" : "5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg"
}
```


<a name="mdesverifycardusingpost"></a>
### Verify Card Details.
```
POST /card/verify/{uuid}
```


#### Description
Verifies the card details (account number) for a provisioned card.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier.|string|
|**Body**|**pan**  <br>*required*|Card number (Primary Account Number).|[PanDTO](#mdespandto)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|object|
|**400**|Possibe causes:<br>Invalid UUID.<br>Account UUID mismatch.<br>The range the account number belongs to doesn't support combo cards.|No Content|
|**401**|Possibe causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* verify


#### Example HTTP request

##### Request path
```
/card/verify/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg
```


##### Request body
```json
{
  "accountNumber" : "5204736610784748"
}
```


#### Example HTTP response

##### Response 200
```json
"object"
```


<a name="mdesupdatecardusingpost"></a>
### Update Card.
```
POST /card/{uuid}
```


#### Description
Updates a card's account number while maintaining all controls associated with the card.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier.|string|
|**Body**|**pan**  <br>*required*|Card number (Primary Account Number).|[PanDTO](#mdespandto)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|object|
|**400**|Possibe causes:<br>Invalid Account Number.<br>Invalid UUID.|No Content|
|**401**|Possibe causes:<br>ClientID not provisioned.<br>Account Number not allowed to be provisioned for the registered ICA associated with ClientID.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|No cards provisioned for the UUID.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* Card


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg
```


##### Request body
```json
{
  "accountNumber" : "5204736610784748"
}
```


#### Example HTTP response

##### Response 200
```json
"object"
```


<a name="mdesretrievepanusingget"></a>
### Retrieve PAN for a given UUID
```
GET /card/{uuid}
```


#### Description
Retrieves the PAN for a registered card.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier.|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[PanDTO](#mdespandto)|
|**400**|Possibe causes:<br>Invalid UUID.<br>Account UUID mismatch.<br>The range the account number belongs to does not support combo cards.|No Content|
|**401**|Possibe causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* Card


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg
```


#### Example HTTP response

##### Response 200
```json
{
  "accountNumber" : "5204736610784748"
}
```


<a name="mdesunregistercardusingdelete"></a>
### Delete Card.
```
DELETE /card/{uuid}
```


#### Description
Deletes a provisioned card and removes all controls associated with it.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier.|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|object|
|**400**|Invalid UUID.|No Content|
|**401**|Possibe causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|No cards provisioned for the UUID.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* Card


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg
```


#### Example HTTP response

##### Response 200
```json
"object"
```


<a name="mdescontrolsusingpost"></a>
### Create or Update Controls.
```
POST /card/{uuid}/controls
```


#### Description
Creates or updates all controls for a provisioned card.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier.|string|
|**Body**|**controls**  <br>*required*|A map containing a list of alert controls and a list of decline controls.|[ControlsDTO_wrapper](#mdescontrolsdto_wrapper)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|object|
|**400**|Possibe causes:<br>Invalid UUID.<br>Invalid amount provided.<br>Empty alerts list provided.<br>Invalid channels provided.<br>Invalid alert type provided.<br>Invalid decline type provided.<br>Empty declines list provided.<br>The controls object doesn't contain any alerts or controls.|No Content|
|**401**|Possibe causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|No cards provisioned for the UUID.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* controls


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls
```


##### Request body
```json
{
  "controls" : {
    "alerts" : [ {
      "type" : "string",
      "amount" : "string",
      "ignoreRecurring" : true,
      "blackWhite" : "black",
      "mccs" : [ "string" ],
      "locations" : [ "string" ],
      "channels" : [ "ATM" ]
    } ],
    "declines" : [ {
      "type" : "string",
      "amount" : "string",
      "ignoreRecurring" : true,
      "blackWhite" : "black",
      "mccs" : [ "string" ],
      "locations" : [ "string" ],
      "channels" : [ "ATM" ]
    } ]
  }
}
```


#### Example HTTP response

##### Response 200
```json
"object"
```


<a name="mdescontrolsusingget"></a>
### Retrieve Controls.
```
GET /card/{uuid}/controls
```


#### Description
Retrieves the details of all controls for a provisioned Card.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier.|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[ControlsDTO_wrapper](#mdescontrolsdto_wrapper)|
|**400**|Invalid UUID.|No Content|
|**401**|Possibe causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|Possibe causes:<br>No cards provisioned for the UUID.<br>No controls are setup for the card.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* controls


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls
```


#### Example HTTP response

##### Response 200
```json
{
  "controls" : {
    "alerts" : [ {
      "type" : "string",
      "amount" : "string",
      "ignoreRecurring" : true,
      "blackWhite" : "black",
      "mccs" : [ "string" ],
      "locations" : [ "string" ],
      "channels" : [ "ATM" ]
    } ],
    "declines" : [ {
      "type" : "string",
      "amount" : "string",
      "ignoreRecurring" : true,
      "blackWhite" : "black",
      "mccs" : [ "string" ],
      "locations" : [ "string" ],
      "channels" : [ "ATM" ]
    } ]
  }
}
```


<a name="mdescontrolsusingdelete"></a>
### Remove Controls.
```
DELETE /card/{uuid}/controls
```


#### Description
Removes all controls for a provisioned card.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier.|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|object|
|**400**|Invalid UUID.|No Content|
|**401**|Possibe causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|Possibe causes:<br>No cards provisioned for the UUID.<br>No controls are setup for the card.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* controls


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls
```


#### Example HTTP response

##### Response 200
```json
"object"
```


<a name="mdesalertsusingpost"></a>
### Creates or Updates Alert Controls:
```
POST /card/{uuid}/controls/alerts
```


#### Description
Creates or updates all Alert controls for a provisioned card without effecting any decline controls.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier.|string|
|**Body**|**alerts**  <br>*required*|List of controls. Each control has a type followed by the regular parameters for the control.|[AlertsDTO](#mdesalertsdto)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|object|
|**400**|Possibe causes:<br>Invalid UUID.<br>Invalid amount provided.<br>Invalid channels provided.<br>Invalid alert type provided.<br>Empty alerts list provided.|No Content|
|**401**|Possibe causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|No cards provisioned for the UUID.|No Content|


#### Consumes

* `application/json`


#### Tags

* alerts


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/alerts
```


##### Request body
```json
{
  "alerts" : [ {
    "type" : "string",
    "amount" : "string",
    "ignoreRecurring" : true,
    "blackWhite" : "black",
    "mccs" : [ "string" ],
    "locations" : [ "string" ],
    "channels" : [ "ATM" ]
  } ]
}
```


#### Example HTTP response

##### Response 200
```json
"object"
```


<a name="mdesalertsusingget"></a>
### Retrieve Alert Controls
```
GET /card/{uuid}/controls/alerts
```


#### Description
Retrieves the details of all Alert controls for a provisioned card.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier.|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[AlertsDTO](#mdesalertsdto)|
|**400**|Invalid UUID.|No Content|
|**401**|Possibe causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|Possibe causes:<br>No cards provisioned for the UUID. <br>No alert controls are setup for the card.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* alerts


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/alerts
```


#### Example HTTP response

##### Response 200
```json
{
  "alerts" : [ {
    "type" : "string",
    "amount" : "string",
    "ignoreRecurring" : true,
    "blackWhite" : "black",
    "mccs" : [ "string" ],
    "locations" : [ "string" ],
    "channels" : [ "ATM" ]
  } ]
}
```


<a name="mdesalertsusingdelete"></a>
### Remove Alert Controls
```
DELETE /card/{uuid}/controls/alerts
```


#### Description
Removes all Alert controls for a provisioned card without effecting any decline controls.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier.|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|object|
|**400**|Invalid UUID.|No Content|
|**401**|Possibe causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|Possibe causes:<br>No cards provisioned for the UUID. <br>No alert controls are setup for the card.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* alerts


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/alerts
```


#### Example HTTP response

##### Response 200
```json
"object"
```


<a name="mdesalertallusingpost"></a>
### Creates or updates the Alerts All control.
```
POST /card/{uuid}/controls/alerts/all
```


#### Description
Creates or updates the Alerts All control for a provisioned card without effecting any other controls.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier|string|
|**Body**|**alertsAll**  <br>*optional*|Alerts All Object|[AlertsAllDTO](#mdesalertsalldto)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|object|
|**400**|Possibe causes:<br>Invalid UUID.<br>Invalid value for Ignore Recurring Authorizations parameter.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|No cards provisioned for the UUID.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* alert-all


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/alerts/all
```


##### Request body
```json
{
  "type" : "string",
  "ignoreRecurring" : false
}
```


#### Example HTTP response

##### Response 200
```json
"object"
```


<a name="mdesalertallusingget"></a>
### Retrieves the details of the Alerts All control.
```
GET /card/{uuid}/controls/alerts/all
```


#### Description
Retrieves the details of the Alerts All control for a provisioned Card.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[AlertsAllDTO](#mdesalertsalldto)|
|**400**|Invalid UUID.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|Possibe causes:<br>No cards provisioned for the UUID.<br>Alerts All control not setup for the card.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* alert-all


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/alerts/all
```


#### Example HTTP response

##### Response 200
```json
{
  "type" : "string",
  "ignoreRecurring" : false
}
```


<a name="mdesalertallusingdelete"></a>
### Removes the Alerts All control.
```
DELETE /card/{uuid}/controls/alerts/all
```


#### Description
Removes the Alerts All control for a provisioned card without effecting any other controls.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|object|
|**400**|Invalid UUID.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|Possible causes:<br>No cards provisioned for the UUID.<br>Alerts All control not setup for the card.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* alert-all


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/alerts/all
```


#### Example HTTP response

##### Response 200
```json
"object"
```


<a name="mdesbudgetalertsusingpost"></a>
### Creates or updates the Budget Alert control.
```
POST /card/{uuid}/controls/alerts/budgets
```


#### Description
Creates or updates the Budget Alert control for a provisioned card without effecting any other controls.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier.|string|
|**Body**|**budgetDTOToAdd**  <br>*required*|Budget Alert Object|[AddBudgetDTO](#mdesaddbudgetdto)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[budgetId](#mdesbudgetid)|
|**400**|Possible causes:<br>Invalid UUID.<br>Invalid channels provided.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|No cards provisioned for the UUID.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* budget-alert


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/alerts/budgets
```


##### Request body
```json
{
  "filter" : [ {
    "type" : "All"
  } ],
  "from" : "2019-05-01T19:03:11+0000",
  "period" : "daily",
  "threshold" : "2093001"
}
```


#### Example HTTP response

##### Response 200
```json
{
  "budgetId" : "string"
}
```


<a name="mdesbudgetalertsusingget"></a>
### Retrieves the details of the Budgets alert control.
```
GET /card/{uuid}/controls/alerts/budgets
```


#### Description
Retrieves the details of the Budgets alert control for a provisioned card without effecting any other controls.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[AbstractBudgetsDTO](#mdesabstractbudgetsdto)|
|**400**|Invalid UUID.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|Possible causes:<br>No cards provisioned for the UUID.<br>Channels decline control not setup for the card.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* budget-alert


#### Example HTTP request

##### Request path
```
/card/string/controls/alerts/budgets
```


#### Example HTTP response

##### Response 200
```json
{
  "budgets" : [ {
    "budgetId" : "string",
    "filter" : [ {
      "type" : "All"
    } ],
    "from" : "2019-05-01T19:03:11+0000",
    "lastUpdated" : "2017-05-18T13:40:40+0000",
    "spendToDate" : "500",
    "period" : "string",
    "threshold" : "string"
  } ],
  "type" : "string"
}
```


<a name="mdesbudgetalertsusingpostbudgetid"></a>
### Creates or updates the Budget Alert control.
```
POST /card/{uuid}/controls/alerts/budgets/{budgetId}
```


#### Description
Creates or updates the Budget Alert control for a provisioned card without effecting any other controls.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**budgetId**  <br>*required*|Card Identifier.|string|
|**Path**|**uuid**  <br>*required*|Card Identifier.|string|
|**Body**|**budgetDTOToAdd**  <br>*required*|Budget alert Object|[BudgetDTO](#mdesbudgetdto)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|object|
|**400**|Possible causes:<br>Invalid UUID.<br>Invalid channels provided.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|No cards provisioned for the UUID.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* budget-alert-with-budgetId


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/alerts/budgets/7c042b1d-e22c-4eff-9187-4721c93a60ab
```


##### Request body
```json
{
  "budgetId" : "string",
  "filter" : [ {
    "type" : "All"
  } ],
  "from" : "2019-05-01T19:03:11+0000",
  "period" : "string",
  "threshold" : "2093001"
}
```


#### Example HTTP response

##### Response 200
```json
"object"
```


<a name="mdesbudgetalertusinggetwithbudgetid"></a>
### Retrieves the details of the Budget Alert control.
```
GET /card/{uuid}/controls/alerts/budgets/{budgetId}
```


#### Description
Retrieves the details of the Budget Alert control for a provisioned card without effecting any other controls.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**budgetId**  <br>*required*|Budget Identifier|string|
|**Path**|**uuid**  <br>*required*|Card Identifier|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[BudgetResponseDTO](#mdesbudgetresponsedto)|
|**400**|Invalid UUID.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|Possible causes:<br>No cards provisioned for the UUID.<br>Channels alert control not setup for the card.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* budget-alert-with-budgetId


#### Example HTTP request

##### Request path
```
/card/string/controls/alerts/budgets/string
```


#### Example HTTP response

##### Response 200
```json
{
  "budgetId" : "string",
  "filter" : [ {
    "type" : "All"
  } ],
  "from" : "2019-05-01T19:03:11+0000",
  "lastUpdated" : "2017-05-18T13:40:40+0000",
  "spendToDate" : "500",
  "period" : "string",
  "threshold" : "string"
}
```


<a name="mdesbudgetalertusingdelete"></a>
### Removes the Budget Alert control.
```
DELETE /card/{uuid}/controls/alerts/budgets/{budgetId}
```


#### Description
Removes the Budget Alert control for a provisioned card without effecting any other controls.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**budgetId**  <br>*required*|Budget Identifier|string|
|**Path**|**uuid**  <br>*required*|Card Identifier|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|object|
|**400**|Invalid UUID.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|Possible causes:<br>No cards provisioned for the UUID.<br>Channels alert control not setup for the card.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* budget-alert-with-budgetId


#### Example HTTP request

##### Request path
```
/card/string/controls/alerts/budgets/string
```


#### Example HTTP response

##### Response 200
```json
"object"
```


<a name="mdeschannelsalertusingpost"></a>
### Creates or updates the Channels Alert control.
```
POST /card/{uuid}/controls/alerts/channels
```


#### Description
Creates or updates the Channels Alert control for a provisioned card without effecting any other controls.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier|string|
|**Body**|**channelAlert**  <br>*required*|Channel Alert Object|[ChannelAlertDTO](#mdeschannelalertdto)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|object|
|**400**|Possible causes:<br>Invalid UUID.<br>Invalid channels provided.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|No cards provisioned for the UUID.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* channel


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/alerts/channels
```


##### Request body
```json
{
  "channels" : [ "ATM" ],
  "type" : "channels",
  "ignoreRecurring" : "true"
}
```


#### Example HTTP response

##### Response 200
```json
"object"
```


<a name="mdeschannelsalertusingget"></a>
### Retrieves the details of the Channels Alert control.
```
GET /card/{uuid}/controls/alerts/channels
```


#### Description
Retrieves the details of the Channels Alert control for a provisioned Card.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[ChannelAlertDTO](#mdeschannelalertdto)|
|**400**|Invalid UUID.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|Possible causes:<br>No cards provisioned for the UUID.<br>Channels alert control not setup for the card.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* channel


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/alerts/channels
```


#### Example HTTP response

##### Response 200
```json
{
  "channels" : [ "ATM" ],
  "type" : "channels",
  "ignoreRecurring" : "true"
}
```


<a name="mdeschannelsalertusingdelete"></a>
### Removes the Channels Alert control.
```
DELETE /card/{uuid}/controls/alerts/channels
```


#### Description
Removes the Channels Alert control for a provisioned card without effecting any other controls.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|object|
|**400**|Invalid UUID.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|Possible causes:<br>No cards provisioned for the UUID.<br>Channels alert control not setup for the card.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* channel


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/alerts/channels
```


#### Example HTTP response

##### Response 200
```json
"object"
```


<a name="mdescrossborderalertusingpost"></a>
### Creates a cross border alert control.
```
POST /card/{uuid}/controls/alerts/crossborder
```


#### Description
Creates or updates the Cross Border Alert control for a provisioned card without effecting any other controls.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier.|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|object|
|**400**|Invalid UUID.|No Content|
|**401**|Possibe causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|No cards provisioned for the UUID.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* cross-border


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/alerts/crossborder
```


#### Example HTTP response

##### Response 200
```json
"object"
```


<a name="mdescrossborderalertusingget"></a>
### Retrieves the details of the Cross Border Alert control.
```
GET /card/{uuid}/controls/alerts/crossborder
```


#### Description
Retrieves the details of the Cross Border Alert control for a provisioned Card.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier.|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|object|
|**400**|Invalid UUID.|No Content|
|**401**|Possibe causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|Possibe causes:<br>No cards provisioned for the UUID. <br>Cross Border alert control not setup for the card.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* cross-border


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/alerts/crossborder
```


#### Example HTTP response

##### Response 200
```json
"object"
```


<a name="mdescrossborderalertusingdelete"></a>
### Removes the Cross Border Alert control for a provisioned card.
```
DELETE /card/{uuid}/controls/alerts/crossborder
```


#### Description
Removes the Cross Border Alert control for a provisioned card without effecting any other controls.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier.|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|object|
|**400**|Invalid UUID.|No Content|
|**401**|Possibe causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|Possibe causes:<br>No cards provisioned for the UUID. <br>Cross Border alert control not setup for the card.<br>  x-samples:|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* cross-border


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/alerts/crossborder
```


#### Example HTTP response

##### Response 200
```json
"object"
```


<a name="mdesfilteralertsusingpost"></a>
### Creates the Combinations specific Alert control.
```
POST /card/{uuid}/controls/alerts/filters
```


#### Description
Creates Combinations specific Alert control for a provisioned card


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier.|string|
|**Body**|**combinationCtrlsDTO**  <br>*required*|Combinations specific Alert Object|[CombinationCtrlsDTO](#mdescombinationctrlsdto)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[CombinationCtrlsResponseDTO](#mdescombinationctrlsresponsedto)|
|**400**|Possible causes:<br>Invalid UUID.<br>Invalid filters provided.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|No cards provisioned for the UUID.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* combination-ctrls-alert-resource


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/alerts/filters
```


##### Request body
```json
{
  "blackWhite" : "string",
  "filter" : [ {
    "type" : "string",
    "amount" : "string",
    "ignoreRecurring" : true,
    "blackWhite" : "black",
    "mccs" : [ "string" ],
    "locations" : [ "string" ],
    "channels" : [ "ATM" ]
  } ]
}
```


#### Example HTTP response

##### Response 200
```json
{
  "blackWhite" : "string",
  "filter" : [ {
    "type" : "string",
    "amount" : "string",
    "ignoreRecurring" : true,
    "blackWhite" : "black",
    "mccs" : [ "string" ],
    "locations" : [ "string" ],
    "channels" : [ "ATM" ]
  } ],
  "filterId" : "string"
}
```


<a name="mdesfilteralertsusingget"></a>
### retrieves all filter controls.
```
GET /card/{uuid}/controls/alerts/filters
```


#### Description
retrieves all filter controls for a provisioned card


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier.|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|List of all filter controls for a specified UUID|[AbstractCombinationCtrlsDTO](#mdesabstractcombinationctrlsdto)|
|**400**|Invalid UUID.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|Possible causes:<br>No cards provisioned for the UUID.<br>Channels decline control not setup for the card.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* combination-ctrls-alert-resource


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/alerts/filters
```


#### Example HTTP response

##### Response 200
```json
{
  "filters" : [ {
    "blackWhite" : "string",
    "filter" : [ {
      "type" : "string",
      "amount" : "string",
      "ignoreRecurring" : true,
      "blackWhite" : "black",
      "mccs" : [ "string" ],
      "locations" : [ "string" ],
      "channels" : [ "ATM" ]
    } ],
    "filterId" : "string"
  } ],
  "lastUpdated" : "string",
  "type" : "string",
  "filterId" : "string",
  "uuid" : "string"
}
```


<a name="mdesfilteralertsusingpost_1"></a>
### updates the Combinations specific Alert control.
```
POST /card/{uuid}/controls/alerts/filters/{filterId}
```


#### Description
Updates Combinations specific Alert control for a provisioned card


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**filterId**  <br>*required*|Filter Identifier.|string|
|**Path**|**uuid**  <br>*required*|Card Identifier.|string|
|**Body**|**combinationCtrlsDTO**  <br>*required*|Combinations specific Alert Object|[CombinationCtrlsDTO](#mdescombinationctrlsdto)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|object|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* combination-ctrls-alert-resource-filterId


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/alerts/filters/fa4e534f-0194-4e8a-bbd6-35f2cfd15825
```


##### Request body
```json
{
  "blackWhite" : "string",
  "filter" : [ {
    "type" : "string",
    "amount" : "string",
    "ignoreRecurring" : true,
    "blackWhite" : "black",
    "mccs" : [ "string" ],
    "locations" : [ "string" ],
    "channels" : [ "ATM" ]
  } ]
}
```


#### Example HTTP response

##### Response 200
```json
"object"
```


<a name="mdesfilteralertsusingget_1"></a>
### retrieves the Combinations specific Alert control.
```
GET /card/{uuid}/controls/alerts/filters/{filterId}
```


#### Description
retrieves a Combinations specific Alert control for the filterID


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**filterId**  <br>*required*|Filter Identifier.|string|
|**Path**|**uuid**  <br>*required*|Card Identifier.|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[AbstractCombinationCtrlsDTO](#mdesabstractcombinationctrlsdto)|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* combination-ctrls-alert-resource-filterId


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/alerts/filters/fa4e534f-0194-4e8a-bbd6-35f2cfd15825
```


#### Example HTTP response

##### Response 200
```json
{
  "filters" : [ {
    "blackWhite" : "string",
    "filter" : [ {
      "type" : "string",
      "amount" : "string",
      "ignoreRecurring" : true,
      "blackWhite" : "black",
      "mccs" : [ "string" ],
      "locations" : [ "string" ],
      "channels" : [ "ATM" ]
    } ],
    "filterId" : "string"
  } ],
  "lastUpdated" : "string",
  "type" : "string",
  "filterId" : "string",
  "uuid" : "string"
}
```


<a name="mdesfilteralertsusingdelete"></a>
### Deletes the Combinations specific Alert control.
```
DELETE /card/{uuid}/controls/alerts/filters/{filterId}
```


#### Description
deletes Combinations specific Alert control for the specific filterID


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**filterId**  <br>*required*|Filter Identifier|string|
|**Path**|**uuid**  <br>*required*|Card Identifier.|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|object|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* combination-ctrls-alert-resource-filterId


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/alerts/filters/fa4e534f-0194-4e8a-bbd6-35f2cfd15825
```


#### Example HTTP response

##### Response 200
```json
"object"
```


<a name="mdesgeolocationsalertusingpost"></a>
### Creates or updates the Geolocations Alert control.
```
POST /card/{uuid}/controls/alerts/geolocations
```


#### Description
Creates or updates the Geolocations Alert control for a provisioned card without effecting any other controls.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier|string|
|**Body**|**geolocationAlert**  <br>*required*|Geolocation Alert Object|[GeolocationAlertDTO](#mdesgeolocationalertdto)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|object|
|**400**|Possible causes:<br>Invalid UUID.<br>Invalid geolocations provided.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|No cards provisioned for the UUID.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* geolocation-alert


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/alerts/geolocations
```


##### Request body
```json
{
  "locations" : [ "string" ],
  "blackWhite" : "black",
  "type" : "string"
}
```


#### Example HTTP response

##### Response 200
```json
"object"
```


<a name="mdesgeolocationsalertusingget"></a>
### Retrieves the details of the Geolocations Alert control.
```
GET /card/{uuid}/controls/alerts/geolocations
```


#### Description
Retrieves the details of the Geolocations Alert control for a provisioned Card.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[GeolocationAlertDTO](#mdesgeolocationalertdto)|
|**400**|Invalid UUID.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|Possible causes:<br>No cards provisioned for the UUID.<br>Geolocations alert control not setup for the card.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* geolocation-alert


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/alerts/geolocations
```


#### Example HTTP response

##### Response 200
```json
{
  "locations" : [ "string" ],
  "blackWhite" : "black",
  "type" : "string"
}
```


<a name="mdesgeolocationsalertusingdelete"></a>
### Removes the Geolocations Alert control.
```
DELETE /card/{uuid}/controls/alerts/geolocations
```


#### Description
Removes the Geolocations Alert control for a provisioned card without effecting any other controls.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|object|
|**400**|Invalid UUID.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|Possible causes:<br>No cards provisioned for the UUID.<br>Geolocations alert control not setup for the card.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* geolocation-alert


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/alerts/geolocations
```


#### Example HTTP response

##### Response 200
```json
"object"
```


<a name="mdesmerchantsalertusingpost"></a>
### Creates or updates the Merchants Alert control.
```
POST /card/{uuid}/controls/alerts/mccs
```


#### Description
Creates or updates the Merchants Alert control for a provisioned card without effecting any other controls.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier|string|
|**Body**|**merchantAlert**  <br>*required*|Merchant Alert Object|[MerchantAlertDTO](#mdesmerchantalertdto)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|object|
|**400**|Possible causes:<br>Invalid UUID.<br>Invalid merchants provided.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|No cards provisioned for the UUID.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* merchant-category-code-alert


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/alerts/mccs
```


##### Request body
```json
{
  "blackWhite" : "black",
  "mccs" : [ "string" ],
  "type" : "string"
}
```


#### Example HTTP response

##### Response 200
```json
"object"
```


<a name="mdesmerchantsalertusingget"></a>
### Retrieves the details of the Merchants Alert control.
```
GET /card/{uuid}/controls/alerts/mccs
```


#### Description
Retrieves the details of the Merchants Alert control for a provisioned Card.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[MerchantAlertDTO](#mdesmerchantalertdto)|
|**400**|Invalid UUID.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|Possible causes:<br>No cards provisioned for the UUID.<br>Merchants alert control not setup for the card.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* merchant-category-code-alert


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/alerts/mccs
```


#### Example HTTP response

##### Response 200
```json
{
  "blackWhite" : "black",
  "mccs" : [ "string" ],
  "type" : "string"
}
```


<a name="mdesmerchantsalertusingdelete"></a>
### Removes the Merchants Alert control.
```
DELETE /card/{uuid}/controls/alerts/mccs
```


#### Description
Removes the Merchants Alert control for a provisioned card without effecting any other controls.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|object|
|**400**|Invalid UUID.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|Possible causes:<br>No cards provisioned for the UUID.<br>Merchants alert control not setup for the card.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* merchant-category-code-alert


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/alerts/mccs
```


#### Example HTTP response

##### Response 200
```json
"object"
```


<a name="mdestransactionamountusingpost"></a>
### Creates a transaction amount alert control.
```
POST /card/{uuid}/controls/alerts/transactionamount
```


#### Description
Creates or updates the Transaction Amount Alert control for a provisioned card without effecting any other controls.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier.|string|
|**Body**|**amountAlert**  <br>*required*|The amount limit (in cardholder base currency).|[AmountAlertDTO](#mdesamountalertdto)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|object|
|**400**|Possibe causes:<br>Invalid UUID.<br>Invalid amount provided.|No Content|
|**401**|Possibe causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|No cards provisioned for the UUID.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* amount


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/alerts/transactionamount
```


##### Request body
```json
{
  "amount" : "10.0",
  "type" : "transactionAmount"
}
```


#### Example HTTP response

##### Response 200
```json
"object"
```


<a name="mdestransactionamountusingget"></a>
### Retrieve Transaction Amount Alert.
```
GET /card/{uuid}/controls/alerts/transactionamount
```


#### Description
Retrieves the details of the Transaction Amount Alert control for a provisioned Card.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier.|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[AmountAlertDTO](#mdesamountalertdto)|
|**400**|Invalid UUID.|No Content|
|**401**|Possibe causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|Possibe causes:<br>No cards provisioned for the UUID. <br>Transaction Amount alert control not setup for the card.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* amount


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/alerts/transactionamount
```


#### Example HTTP response

##### Response 200
```json
{
  "amount" : "10.0",
  "type" : "transactionAmount"
}
```


<a name="mdestransactionamountusingdelete"></a>
### Remove Transaction Amount Alert.
```
DELETE /card/{uuid}/controls/alerts/transactionamount
```


#### Description
Removes the Transaction Amount Alert control for a provisioned card without effecting any other controls.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier.|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|object|
|**400**|Invalid UUID.|No Content|
|**401**|Possibe causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|Possibe causes:<br>No cards provisioned for the UUID. <br>Transaction Amount alert control not setup for the card.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* amount


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/alerts/transactionamount
```


#### Example HTTP response

##### Response 200
```json
"object"
```


<a name="mdestravelalertsusingpost"></a>
### Creates or updates the Travel Alert control.
```
POST /card/{uuid}/controls/alerts/travels
```


#### Description
Creates the Travel Alert control for a provisioned card without effecting any other controls.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier.|string|
|**Body**|**travelDTO**  <br>*required*|Travel Alert Object.|[TravelDTO](#mdestraveldto)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[travelId](#mdestravelid)|
|**400**|Invalid UUID.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|Possible causes:<br>No cards provisioned for the UUID.<br>Channels alert control not setup for the card.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* Travel-Alert


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/alerts/travels
```


##### Request body
```json
{
  "locations" : [ "string" ],
  "blackWhite" : "black",
  "alias" : "Summer Holidays",
  "validFrom" : "2022-03-29T13:08:33+0100",
  "validTo" : "2022-05-29T13:08:33+0100"
}
```


#### Example HTTP response

##### Response 200
```json
{
  "travelId" : "string"
}
```


<a name="mdestravelalertsusingget"></a>
### Retrieves all the details of the Travel alert control.
```
GET /card/{uuid}/controls/alerts/travels
```


#### Description
Retrieves  all the details of the Travel alert control for a provisioned card without effecting any other controls.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier.|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[TravelListResponseDTO](#mdestravellistresponsedto)|
|**400**|Invalid UUID.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|Possible causes:<br>No cards provisioned for the UUID.<br>Channels decline control not setup for the card.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* Travel-Alert


#### Example HTTP request

##### Request path
```
/card/string/controls/alerts/travels
```


#### Example HTTP response

##### Response 200
```json
{
  "travels" : [ {
    "travelId" : "65d59c64-8d12-4bca-a3a2-52f32d35f91d",
    "locations" : [ "string" ],
    "blackWhite" : "black",
    "alias" : "Summer Holidays",
    "validFrom" : "2022-03-29T13:08:33+0100",
    "validTo" : "2022-05-29T13:08:33+0100"
  } ]
}
```


<a name="mdestravelalertsusingposttravelid"></a>
### Creates or updates the Travel Alert control.
```
POST /card/{uuid}/controls/alerts/travels/{travelId}
```


#### Description
Updates the Travel alert control for a provisioned card without effecting any other controls.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**travelId**  <br>*required*|Travel Identifier.|string|
|**Path**|**uuid**  <br>*required*|Card Identifier.|string|
|**Body**|**travelDTO**  <br>*required*|Travel alert Object.|[TravelDTO](#mdestraveldto)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|object|
|**400**|Possible causes:<br>Invalid UUID.<br>Invalid channels provided.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|No cards provisioned for the UUID.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* Travel-Alert


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/alerts/travels/65d59c64-8d12-4bca-a3a2-52f32d35f91d
```


##### Request body
```json
{
  "locations" : [ "string" ],
  "blackWhite" : "black",
  "alias" : "Summer Holidays",
  "validFrom" : "2022-03-29T13:08:33+0100",
  "validTo" : "2022-05-29T13:08:33+0100"
}
```


#### Example HTTP response

##### Response 200
```json
"object"
```


<a name="mdestravelalertsusinggettravelid"></a>
### Retrieves the details of the Travel alert control.
```
GET /card/{uuid}/controls/alerts/travels/{travelId}
```


#### Description
Retrieves the details of the Travel alert control for a provisioned card without effecting any other controls.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**travelId**  <br>*required*|Travel Identifier.|string|
|**Path**|**uuid**  <br>*required*|Card Identifier.|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[TravelResponseDTO](#mdestravelresponsedto)|
|**400**|Invalid UUID.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|Possible causes:<br>No cards provisioned for the UUID.<br>Channels decline control not setup for the card.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* Travel-Alert


#### Example HTTP request

##### Request path
```
/card/string/controls/alerts/travels/65d59c64-8d12-4bca-a3a2-52f32d35f91d
```


#### Example HTTP response

##### Response 200
```json
{
  "travelId" : "65d59c64-8d12-4bca-a3a2-52f32d35f91d",
  "locations" : [ "string" ],
  "blackWhite" : "black",
  "alias" : "Summer Holidays",
  "validFrom" : "2022-03-29T13:08:33+0100",
  "validTo" : "2022-05-29T13:08:33+0100"
}
```


<a name="mdestravelalertusingdelete"></a>
### Removes the Travel alert control.
```
DELETE /card/{uuid}/controls/alerts/travels/{travelId}
```


#### Description
Removes the Travel alert control for a provisioned card without effecting any other controls.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**travelId**  <br>*required*|Travel Identifier.|string|
|**Path**|**uuid**  <br>*required*|Card Identifier.|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|object|
|**400**|Invalid UUID.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|Possible causes:<br>No cards provisioned for the UUID.<br>Channels alert control not setup for the card.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* Travel-Alert


#### Example HTTP request

##### Request path
```
/card/string/controls/alerts/travels/string
```


#### Example HTTP response

##### Response 200
```json
"object"
```


<a name="mdesdeclinecontrolsusingpost"></a>
### Creates or updates all Decline controls
```
POST /card/{uuid}/controls/declines
```


#### Description
Creates or updates all Decline controls for a provisioned card without effecting any alert controls.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier|string|
|**Body**|**declines**  <br>*required*|Declines Object|[DeclinesDTO](#mdesdeclinesdto)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|object|
|**400**|Possible causes:<br>Invalid UUID.<br>Invalid decline type provided.<br>Empty declines list provided.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|No cards provisioned for the UUID.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* declines


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/declines
```


##### Request body
```json
{
  "declines" : [ {
    "type" : "string",
    "amount" : "string",
    "ignoreRecurring" : true,
    "blackWhite" : "black",
    "mccs" : [ "string" ],
    "locations" : [ "string" ],
    "channels" : [ "ATM" ]
  } ]
}
```


#### Example HTTP response

##### Response 200
```json
"object"
```


<a name="mdesdeclinecontrolsusingget"></a>
### Retrieves the details of all Decline controls
```
GET /card/{uuid}/controls/declines
```


#### Description
Retrieves the details of all Decline controls for a provisioned Card.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[DeclinesDTO](#mdesdeclinesdto)|
|**400**|Invalid UUID.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|Possible causes:<br>No cards provisioned for the UUID.<br>No decline controls are setup for the card.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* declines


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/declines
```


#### Example HTTP response

##### Response 200
```json
{
  "declines" : [ {
    "type" : "string",
    "amount" : "string",
    "ignoreRecurring" : true,
    "blackWhite" : "black",
    "mccs" : [ "string" ],
    "locations" : [ "string" ],
    "channels" : [ "ATM" ]
  } ]
}
```


<a name="mdesdeclinecontrolsusingdelete"></a>
### Removes all Decline controls
```
DELETE /card/{uuid}/controls/declines
```


#### Description
Removes all Decline controls for a provisioned card without effecting any alert controls.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|object|
|**400**|Invalid UUID.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|Possible causes:<br>No cards provisioned for the UUID.<br>No decline controls are setup for the card.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* declines


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/declines
```


#### Example HTTP response

##### Response 200
```json
"object"
```


<a name="mdesdeclineallusingpost"></a>
### This control will decline all authorizations on the card.
```
POST /card/{uuid}/controls/declines/all
```


#### Description
Creates or updates the Disable Card Decline control for a provisioned card without effecting any other controls.  If Ignore Recurring Authorizations flag is provided and set to true, recurring authorizations won't be declined.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier.|string|
|**Body**|**cardDecline**  <br>*optional*|Ignore Recurring Authorizations.|[CardDeclineDTO](#mdescarddeclinedto)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|object|
|**400**|Possibe causes:<br>Invalid UUID.<br>Invalid value for Ignore Recurring Authorizations parameter.|No Content|
|**401**|Possibe causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|No cards provisioned for the UUID.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* decline-all


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/declines/all
```


##### Request body
```json
{
  "type" : "string",
  "ignoreRecurring" : false
}
```


#### Example HTTP response

##### Response 200
```json
"object"
```


<a name="mdesdeclineallusingget"></a>
### Retrieve Card Disable Decline Control.
```
GET /card/{uuid}/controls/declines/all
```


#### Description
Retrieves the details of the Card Disable Decline control for a provisioned Card.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier.|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[CardDeclineDTO](#mdescarddeclinedto)|
|**400**|Invalid UUID|No Content|
|**401**|Possibe causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|Possibe causes:<br>No cards provisioned for the UUID.<br>Card Disable decline control not setup for the card.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* decline-all


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/declines/all
```


#### Example HTTP response

##### Response 200
```json
{
  "type" : "string",
  "ignoreRecurring" : false
}
```


<a name="mdesdeclineallusingdelete"></a>
### Removes the Card Disable Decline control.
```
DELETE /card/{uuid}/controls/declines/all
```


#### Description
Removes the Card Disable Decline control for a provisioned card without effecting any other controls.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier.|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|object|
|**400**|Invalid UUID.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|Possible causes:<br>No cards provisioned for the UUID.<br>Card Disable decline control not setup for the card.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* decline-all


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/declines/all
```


#### Example HTTP response

##### Response 200
```json
"object"
```


<a name="mdesbudgetdeclinesusingpost"></a>
### Creates or updates the Budget decline control.
```
POST /card/{uuid}/controls/declines/budgets
```


#### Description
Creates or updates the Budget decline control for a provisioned card without effecting any other controls.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier.|string|
|**Body**|**budgetDTOToAdd**  <br>*required*|Budget decline Object|[AddBudgetDTO](#mdesaddbudgetdto)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[budgetId](#mdesbudgetid)|
|**400**|Possible causes:<br>Invalid UUID.<br>Invalid channels provided.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|No cards provisioned for the UUID.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* budget-decline


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/declines/budgets
```


##### Request body
```json
{
  "filter" : [ {
    "type" : "All"
  } ],
  "from" : "2019-05-01T19:03:11+0000",
  "period" : "daily",
  "threshold" : "2093001"
}
```


#### Example HTTP response

##### Response 200
```json
{
  "budgetId" : "string"
}
```


<a name="mdesbudgetdeclinesusingget_2"></a>
### Retrieves the details of the Budgets decline control.
```
GET /card/{uuid}/controls/declines/budgets
```


#### Description
Retrieves the details of the Budgets decline control for a provisioned card without effecting any other controls.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[AbstractBudgetsDTO](#mdesabstractbudgetsdto)|
|**400**|Invalid UUID.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|Possible causes:<br>No cards provisioned for the UUID.<br>Channels decline control not setup for the card.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* budget-decline


#### Example HTTP request

##### Request path
```
/card/string/controls/declines/budgets
```


#### Example HTTP response

##### Response 200
```json
{
  "budgets" : [ {
    "budgetId" : "string",
    "filter" : [ {
      "type" : "All"
    } ],
    "from" : "2019-05-01T19:03:11+0000",
    "lastUpdated" : "2017-05-18T13:40:40+0000",
    "spendToDate" : "500",
    "period" : "string",
    "threshold" : "string"
  } ],
  "type" : "string"
}
```


<a name="mdesbudgetdeclinesusingpost_2"></a>
### Creates or updates the Budget decline control.
```
POST /card/{uuid}/controls/declines/budgets/{budgetId}
```


#### Description
Creates or updates the Budget decline control for a provisioned card without effecting any other controls.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**budgetId**  <br>*required*|Budget Identifier|string|
|**Path**|**uuid**  <br>*required*|Card Identifier.|string|
|**Body**|**Budget DTO**  <br>*required*|Budget decline Object|[BudgetDTO](#mdesbudgetdto)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|object|
|**400**|Possible causes:<br>Invalid UUID.<br>Invalid channels provided.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|No cards provisioned for the UUID.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* budget-decline-with-budgetId


#### Example HTTP request

##### Request path
```
/card/string/controls/declines/budgets/string
```


##### Request body
```json
{
  "budgetId" : "string",
  "filter" : [ {
    "type" : "All"
  } ],
  "from" : "2019-05-01T19:03:11+0000",
  "period" : "string",
  "threshold" : "2093001"
}
```


#### Example HTTP response

##### Response 200
```json
"object"
```


<a name="mdesbudgetdeclinesusingget_1"></a>
### Retrieves the details of the Budget decline control.
```
GET /card/{uuid}/controls/declines/budgets/{budgetId}
```


#### Description
Retrieves the details of the Budget decline control for a provisioned card without effecting any other controls.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**budgetId**  <br>*required*|Budget Identifier|string|
|**Path**|**uuid**  <br>*required*|Card Identifier|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[BudgetResponseDTO](#mdesbudgetresponsedto)|
|**400**|Invalid UUID.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|Possible causes:<br>No cards provisioned for the UUID.<br>Channels decline control not setup for the card.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* budget-decline-with-budgetId


#### Example HTTP request

##### Request path
```
/card/string/controls/declines/budgets/string
```


#### Example HTTP response

##### Response 200
```json
{
  "budgetId" : "string",
  "filter" : [ {
    "type" : "All"
  } ],
  "from" : "2019-05-01T19:03:11+0000",
  "lastUpdated" : "2017-05-18T13:40:40+0000",
  "spendToDate" : "500",
  "period" : "string",
  "threshold" : "string"
}
```


<a name="mdesbudgetdeclineusingdelete"></a>
### Removes the Budget decline control.
```
DELETE /card/{uuid}/controls/declines/budgets/{budgetId}
```


#### Description
Removes the Budget decline control for a provisioned card without effecting any other controls.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**budgetId**  <br>*required*|Budget Identifier|string|
|**Path**|**uuid**  <br>*required*|Card Identifier|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|object|
|**400**|Invalid UUID.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|Possible causes:<br>No cards provisioned for the UUID.<br>Channels decline control not setup for the card.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* budget-decline-with-budgetId


#### Example HTTP request

##### Request path
```
/card/string/controls/declines/budgets/string
```


#### Example HTTP response

##### Response 200
```json
"object"
```


<a name="mdeschannelsdeclineusingpost"></a>
### Creates or updates the Channels Decline control.
```
POST /card/{uuid}/controls/declines/channels
```


#### Description
Creates or updates the Channels Decline control for a provisioned card without effecting any other controls.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier|string|
|**Body**|**channelDecline**  <br>*required*|Channel Decline Object|[ChannelDeclineDTO](#mdeschanneldeclinedto)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|object|
|**400**|Possible causes:<br>Invalid UUID.<br>Invalid channels provided.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|No cards provisioned for the UUID.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* channel-decline


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/declines/channels
```


##### Request body
```json
{
  "channels" : [ "ATM" ],
  "type" : "channels",
  "ignoreRecurring" : "true"
}
```


#### Example HTTP response

##### Response 200
```json
"object"
```


<a name="mdeschannelsdeclineusingget"></a>
### Retrieves the details of the Channels Decline control.
```
GET /card/{uuid}/controls/declines/channels
```


#### Description
Retrieves the details of the Channels Decline control for a provisioned Card.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[ChannelDeclineDTO](#mdeschanneldeclinedto)|
|**400**|Invalid UUID.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|Possible causes:<br>No cards provisioned for the UUID.<br>Channels decline control not setup for the card.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* channel-decline


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/declines/channels
```


#### Example HTTP response

##### Response 200
```json
{
  "channels" : [ "ATM" ],
  "type" : "channels",
  "ignoreRecurring" : "true"
}
```


<a name="mdeschannelsdeclineusingdelete"></a>
### Removes the Channels Decline control.
```
DELETE /card/{uuid}/controls/declines/channels
```


#### Description
Removes the Channels Decline control for a provisioned card without effecting any other controls.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|object|
|**400**|Invalid UUID.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|Possible causes:<br>No cards provisioned for the UUID.<br>Channels decline control not setup for the card.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* channel-decline


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/declines/channels
```


#### Example HTTP response

##### Response 200
```json
"object"
```


<a name="mdescrossborderdeclineusingpost"></a>
### Create or Update Cross Border Decline.
```
POST /card/{uuid}/controls/declines/crossborder
```


#### Description
Creates or updates the Cross Border Decline control for a provisioned card without effecting any other controls


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier.|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|object|
|**400**|Invalid UUID.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|No cards provisioned for the UUID.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* cross-border-decline


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/declines/crossborder
```


#### Example HTTP response

##### Response 200
```json
"object"
```


<a name="mdescrossborderdeclineusingget"></a>
### Retrieve Cross Border Decline,
```
GET /card/{uuid}/controls/declines/crossborder
```


#### Description
Retrieves the details of the Cross Border Decline control for a provisioned Card.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier.|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[CrossBorderDeclineDTO](#mdescrossborderdeclinedto)|
|**400**|Invalid UUID.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|Possible causes:<br>No cards provisioned for the UUID.<br>Cross Border decline control not setup for the card.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* cross-border-decline


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/declines/crossborder
```


#### Example HTTP response

##### Response 200
```json
{
  "type" : "string"
}
```


<a name="mdescrossborderdeclineusingdelete"></a>
### Remove Cross Border Decline.
```
DELETE /card/{uuid}/controls/declines/crossborder
```


#### Description
Removes the Cross Border Decline control for a provisioned card without effecting any other controls.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier.|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|object|
|**400**|Invalid UUID.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|Possible causes:<br>No cards provisioned for the UUID.<br>Cross Border decline control not setup for the card.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* cross-border-decline


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/declines/crossborder
```


#### Example HTTP response

##### Response 200
```json
"object"
```


<a name="mdesfilterdeclinesusingpost"></a>
### Creates the Combinations specific Decline control.
```
POST /card/{uuid}/controls/declines/filters
```


#### Description
Creates Combinations specific Decline control for a provisioned card


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier.|string|
|**Body**|**combinationCtrlsDTO**  <br>*required*|Combinations specific Decline Object|[CombinationCtrlsDTO](#mdescombinationctrlsdto)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[CombinationCtrlsResponseDTO](#mdescombinationctrlsresponsedto)|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* combination-ctrls-decline-resource


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/declines/filters
```


##### Request body
```json
{
  "blackWhite" : "string",
  "filter" : [ {
    "type" : "string",
    "amount" : "string",
    "ignoreRecurring" : true,
    "blackWhite" : "black",
    "mccs" : [ "string" ],
    "locations" : [ "string" ],
    "channels" : [ "ATM" ]
  } ]
}
```


#### Example HTTP response

##### Response 200
```json
{
  "blackWhite" : "string",
  "filter" : [ {
    "type" : "string",
    "amount" : "string",
    "ignoreRecurring" : true,
    "blackWhite" : "black",
    "mccs" : [ "string" ],
    "locations" : [ "string" ],
    "channels" : [ "ATM" ]
  } ],
  "filterId" : "string"
}
```


<a name="mdesfilterdeclinesusingget"></a>
### retrieves all filter controls.
```
GET /card/{uuid}/controls/declines/filters
```


#### Description
retrieves all filter controls for a provisioned card


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier.|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|List of all filter controls for a specified UUID|[AbstractCombinationCtrlsDTO](#mdesabstractcombinationctrlsdto)|
|**400**|Invalid UUID.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|Possible causes:<br>No cards provisioned for the UUID.<br>Channels decline control not setup for the card.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* combination-ctrls-decline-resource


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/declines/filters
```


#### Example HTTP response

##### Response 200
```json
{
  "filters" : [ {
    "blackWhite" : "string",
    "filter" : [ {
      "type" : "string",
      "amount" : "string",
      "ignoreRecurring" : true,
      "blackWhite" : "black",
      "mccs" : [ "string" ],
      "locations" : [ "string" ],
      "channels" : [ "ATM" ]
    } ],
    "filterId" : "string"
  } ],
  "lastUpdated" : "string",
  "type" : "string",
  "filterId" : "string",
  "uuid" : "string"
}
```


<a name="mdesfilterdeclinesusingpost_1"></a>
### updates the Combinations specific Decline control.
```
POST /card/{uuid}/controls/declines/filters/{filterId}
```


#### Description
Updates Combinations specific Decline control for a provisioned card


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**filterId**  <br>*required*|Filter Identifier.|string|
|**Path**|**uuid**  <br>*required*|Card Identifier.|string|
|**Body**|**combinationCtrlsDTO**  <br>*required*|Combinations specific Decline Object|[CombinationCtrlsDTO](#mdescombinationctrlsdto)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|object|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* combination-ctrls-decline-resource-filterId


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/declines/filters/fa4e534f-0194-4e8a-bbd6-35f2cfd15825
```


##### Request body
```json
{
  "blackWhite" : "string",
  "filter" : [ {
    "type" : "string",
    "amount" : "string",
    "ignoreRecurring" : true,
    "blackWhite" : "black",
    "mccs" : [ "string" ],
    "locations" : [ "string" ],
    "channels" : [ "ATM" ]
  } ]
}
```


#### Example HTTP response

##### Response 200
```json
"object"
```


<a name="mdesfilterdeclinesusingget_1"></a>
### retrieves the Combinations specific Decline control.
```
GET /card/{uuid}/controls/declines/filters/{filterId}
```


#### Description
retrieves a Combinations specific Decline control for the filterID


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**filterId**  <br>*required*|Filter Identifier.|string|
|**Path**|**uuid**  <br>*required*|Card Identifier.|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[AbstractCombinationCtrlsDTO](#mdesabstractcombinationctrlsdto)|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* combination-ctrls-decline-resource-filterId


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/declines/filters/fa4e534f-0194-4e8a-bbd6-35f2cfd15825
```


#### Example HTTP response

##### Response 200
```json
{
  "filters" : [ {
    "blackWhite" : "string",
    "filter" : [ {
      "type" : "string",
      "amount" : "string",
      "ignoreRecurring" : true,
      "blackWhite" : "black",
      "mccs" : [ "string" ],
      "locations" : [ "string" ],
      "channels" : [ "ATM" ]
    } ],
    "filterId" : "string"
  } ],
  "lastUpdated" : "string",
  "type" : "string",
  "filterId" : "string",
  "uuid" : "string"
}
```


<a name="mdesfilterdeclinesusingdelete"></a>
### Deletes the Combinations specific Decline control.
```
DELETE /card/{uuid}/controls/declines/filters/{filterId}
```


#### Description
deletes Combinations specific Decline control for the specific filterID


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**filterId**  <br>*required*|Filter Identifier|string|
|**Path**|**uuid**  <br>*required*|Card Identifier.|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|object|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* combination-ctrls-decline-resource-filterId


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/declines/filters/fa4e534f-0194-4e8a-bbd6-35f2cfd15825
```


#### Example HTTP response

##### Response 200
```json
"object"
```


<a name="mdesgeolocationsdeclineusingpost"></a>
### Create or Update Geolocations Control.
```
POST /card/{uuid}/controls/declines/geolocations
```


#### Description
Creates or updates the Geolocations control for a provisioned [v]card without effecting any other controls.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier|string|
|**Body**|**geolocation**  <br>*required*|geolocation|[GeolocationDeclineDTO](#mdesgeolocationdeclinedto)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|object|
|**400**|Possible causes:<br>Invalid UUID.<br>The Country Code provided is not valid.<br>The Postal Code provided is not valid.<br>Empty locations list provided.<br>Invalid value for blackWhite flag.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|No cards provisioned for the UUID.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* geolocation-decline


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/declines/geolocations
```


##### Request body
```json
{
  "blackWhite" : "black",
  "locations" : [ "string" ],
  "type" : "string"
}
```


#### Example HTTP response

##### Response 200
```json
"object"
```


<a name="mdesgeolocationsdeclineusingget"></a>
### Retrieve Geolocations Control.
```
GET /card/{uuid}/controls/declines/geolocations
```


#### Description
Retrieves the details of the Geolocations control for a provisioned [v]card.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[GeolocationDeclineDTO](#mdesgeolocationdeclinedto)|
|**400**|Invalid UUID.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|Possible causes:<br>No [v]cards provisioned for the UUID.<br>Geolocations control not setup for the card.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* geolocation-decline


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/declines/geolocations
```


#### Example HTTP response

##### Response 200
```json
{
  "blackWhite" : "black",
  "locations" : [ "string" ],
  "type" : "string"
}
```


<a name="mdesgeolocationsdeclineusingdelete"></a>
### Remove Geolocations Control.
```
DELETE /card/{uuid}/controls/declines/geolocations
```


#### Description
Removes the Channels Decline control for a provisioned card without effecting any other controls.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|object|
|**400**|Invalid UUID.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|Possible causes:<br>No [v]cards provisioned for the UUID.<br>Geolocations control not setup for the card.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* geolocation-decline


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/declines/geolocations
```


#### Example HTTP response

##### Response 200
```json
"object"
```


<a name="mdesmccsdeclineusingpost"></a>
### Create or Update MCCs Control.
```
POST /card/{uuid}/controls/declines/mccs
```


#### Description
Creates or updates the MCCs control for a provisioned card without effecting any other controls.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier|string|
|**Body**|**mcc**  <br>*required*|mcc|[MerchantDeclineDTO](#mdesmerchantdeclinedto)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|object|
|**400**|Possible causes:<br>Invalid UUID.<br>A category code provided is not valid.<br>Empty MCCs list provided.<br>Invalid value for blackWhite flag.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|No cards provisioned for the UUID.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* merchant-category-code-decline


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/declines/mccs
```


##### Request body
```json
{
  "blackWhite" : "black",
  "mccs" : [ "string" ],
  "type" : "string"
}
```


#### Example HTTP response

##### Response 200
```json
"object"
```


<a name="mdesmccsdeclineusingget"></a>
### Retrieve MCCs Control.
```
GET /card/{uuid}/controls/declines/mccs
```


#### Description
Retrieves the details of the MCCs control for a provisioned card.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[MerchantDeclineDTO](#mdesmerchantdeclinedto)|
|**400**|Invalid UUID.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|Possible causes:<br>No cards provisioned for the UUID.<br>MCCs control not setup for the card.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* merchant-category-code-decline


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/declines/mccs
```


#### Example HTTP response

##### Response 200
```json
{
  "blackWhite" : "black",
  "mccs" : [ "string" ],
  "type" : "string"
}
```


<a name="mdesmccsdeclineusingdelete"></a>
### Remove MCCs Control.
```
DELETE /card/{uuid}/controls/declines/mccs
```


#### Description
Removes the MCCs control for a provisioned card without effecting any other controls.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|object|
|**400**|Invalid UUID.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|Possible causes:<br>No cards provisioned for the UUID.<br>MCCs control not setup for the card.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* merchant-category-code-decline


#### Example HTTP request

##### Request path
```
/card/string/controls/declines/mccs
```


#### Example HTTP response

##### Response 200
```json
"object"
```


<a name="mdescreateamountdeclineusingpost"></a>
### Creates a transaction amount decline control.
```
POST /card/{uuid}/controls/declines/transactionamount
```


#### Description
Creates or updates the Transaction Amount Decline control for a provisioned card without effecting any other controls.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier.|string|
|**Body**|**amount**  <br>*required*|The amount limit (in cardholder base currency).|[AmountDeclineDTO](#mdesamountdeclinedto)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[AmountDeclineDTO](#mdesamountdeclinedto)|
|**400**|Invalid UUID.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|No cards provisioned for the UUID. <br>Transaction Amount decline control not setup for the card.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* amount-decline


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/declines/transactionamount
```


##### Request body
```json
{
  "amount" : "10.0",
  "type" : "string"
}
```


#### Example HTTP response

##### Response 200
```json
{
  "amount" : "10.0",
  "type" : "string"
}
```


<a name="mdesgetamountdeclineusingget"></a>
### Retrieve Transaction Amount Decline.
```
GET /card/{uuid}/controls/declines/transactionamount
```


#### Description
Retrieves the details of the Transaction Amount Decline control for a provisioned Card.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier.|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[AmountDeclineDTO](#mdesamountdeclinedto)|
|**400**|Invalid UUID.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|Possible causes:<br>No cards provisioned for the UUID. <br>Transaction Amount decline control not setup for the card.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* amount-decline


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/declines/transactionamount
```


#### Example HTTP response

##### Response 200
```json
{
  "amount" : "10.0",
  "type" : "string"
}
```


<a name="mdesdeleteamountdeclineusingdelete"></a>
### Remove Transaction Amount Decline.
```
DELETE /card/{uuid}/controls/declines/transactionamount
```


#### Description
Removes the Transaction Amount Decline control for a provisioned card without effecting any other controls.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier.|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|object|
|**400**|Invalid UUID.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|Possible causes:<br>No cards provisioned for the UUID. <br>Transaction Amount decline control not setup for the card.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* amount-decline


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/declines/transactionamount
```


#### Example HTTP response

##### Response 200
```json
"object"
```


<a name="mdestraveldeclineusingpost"></a>
### Creates the Travel decline control.
```
POST /card/{uuid}/controls/declines/travels
```


#### Description
Creates the Travel decline control for a provisioned card without effecting any other controls.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier.|string|
|**Body**|**travelDTO**  <br>*required*|Travel Decline Object.|[TravelDTO](#mdestraveldto)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[travelId](#mdestravelid)|
|**400**|Invalid UUID.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|Possible causes:<br>No cards provisioned for the UUID.<br>Channels alert control not setup for the card.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* Travel-Decline


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/declines/travels
```


##### Request body
```json
{
  "locations" : [ "string" ],
  "blackWhite" : "black",
  "alias" : "Summer Holidays",
  "validFrom" : "2022-03-29T13:08:33+0100",
  "validTo" : "2022-05-29T13:08:33+0100"
}
```


#### Example HTTP response

##### Response 200
```json
{
  "travelId" : "string"
}
```


<a name="mdestraveldeclinesusingget"></a>
### Retrieves all the details of the Travel decline control.
```
GET /card/{uuid}/controls/declines/travels
```


#### Description
Retrieves all the details of the Travel decline control for a provisioned card without effecting any other controls.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier.|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[TravelListResponseDTO](#mdestravellistresponsedto)|
|**400**|Invalid UUID.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|Possible causes:<br>No cards provisioned for the UUID.<br>Channels decline control not setup for the card.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* Travel-Decline


#### Example HTTP request

##### Request path
```
/card/string/controls/declines/travels
```


#### Example HTTP response

##### Response 200
```json
{
  "travels" : [ {
    "travelId" : "65d59c64-8d12-4bca-a3a2-52f32d35f91d",
    "locations" : [ "string" ],
    "blackWhite" : "black",
    "alias" : "Summer Holidays",
    "validFrom" : "2022-03-29T13:08:33+0100",
    "validTo" : "2022-05-29T13:08:33+0100"
  } ]
}
```


<a name="mdestraveldeclineusingposttravelid"></a>
### Updates the Travel decline control.
```
POST /card/{uuid}/controls/declines/travels/{travelId}
```


#### Description
Updates the Travel decline control for a provisioned card without effecting any other controls.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**travelId**  <br>*required*|Travel Identifier.|string|
|**Path**|**uuid**  <br>*required*|Card Identifier.|string|
|**Body**|**travelDTO**  <br>*required*|Travel Decline Object.|[TravelDTO](#mdestraveldto)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|object|
|**400**|Possible causes:<br>Invalid UUID.<br>Invalid channels provided.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|No cards provisioned for the UUID.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* Travel-Decline


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/controls/declines/travels/65d59c64-8d12-4bca-a3a2-52f32d35f91d
```


##### Request body
```json
{
  "locations" : [ "string" ],
  "blackWhite" : "black",
  "alias" : "Summer Holidays",
  "validFrom" : "2022-03-29T13:08:33+0100",
  "validTo" : "2022-05-29T13:08:33+0100"
}
```


#### Example HTTP response

##### Response 200
```json
"object"
```


<a name="mdestraveldeclinesusinggettravelid"></a>
### Retrieves the details of the Travel decline control.
```
GET /card/{uuid}/controls/declines/travels/{travelId}
```


#### Description
Retrieves the details of the Travel decline control for a provisioned card without effecting any other controls.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**travelId**  <br>*required*|Travel Identifier.|string|
|**Path**|**uuid**  <br>*required*|Card Identifier.|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[TravelResponseDTO](#mdestravelresponsedto)|
|**400**|Invalid UUID.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|Possible causes:<br>No cards provisioned for the UUID.<br>Channels decline control not setup for the card.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* Travel-Decline


#### Example HTTP request

##### Request path
```
/card/string/controls/declines/travels/65d59c64-8d12-4bca-a3a2-52f32d35f91d
```


#### Example HTTP response

##### Response 200
```json
{
  "travelId" : "65d59c64-8d12-4bca-a3a2-52f32d35f91d",
  "locations" : [ "string" ],
  "blackWhite" : "black",
  "alias" : "Summer Holidays",
  "validFrom" : "2022-03-29T13:08:33+0100",
  "validTo" : "2022-05-29T13:08:33+0100"
}
```


<a name="mdestraveldeclineusingdelete"></a>
### Removes the Travel decline control.
```
DELETE /card/{uuid}/controls/declines/travels/{travelId}
```


#### Description
Removes the Travel decline control for a provisioned card without effecting any other controls.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**travelId**  <br>*required*|Travel Identifier.|string|
|**Path**|**uuid**  <br>*required*|Card Identifier.|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|object|
|**400**|Invalid UUID.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|Possible causes:<br>No cards provisioned for the UUID.<br>Channels alert control not setup for the card.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* Travel-Decline


#### Example HTTP request

##### Request path
```
/card/string/controls/declines/travels/string
```


#### Example HTTP response

##### Response 200
```json
"object"
```


<a name="mdescontrolshistoryusingget"></a>
### Retrieve Paged Controls History.
```
GET /card/{uuid}/history/controls
```


#### Description
Retrieves the details of all controls for a provisioned Card.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier.|string|
|**Query**|**from**  <br>*required*|from|string|
|**Query**|**pageNumber**  <br>*optional*|pageNumber|integer (int32)|
|**Query**|**pageSize**  <br>*optional*|pageSize|integer (int32)|
|**Query**|**to**  <br>*optional*|to|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[ControlsHistoryDTO](#mdescontrolshistorydto)|
|**400**|Possible causes:<br>Invalid UUID.<br>Invalid value for from.<br>Invalid value for to<br>The value for to is less than the value for from.<br>Invalid page number.<br>Invalid page size.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|Possible causes:<br>No cards provisioned for the UUID.<br>No transactions were found matching specified parameters.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* controls-history


#### Example HTTP request

##### Request path
```
/card/5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg/history/controls?from=2017-09-28T00:00:00Z
```


#### Example HTTP response

##### Response 200
```json
{
  "fromDate" : "string",
  "list" : [ {
    "controls" : {
      "alerts" : [ {
        "type" : "string",
        "amount" : "string",
        "ignoreRecurring" : true,
        "blackWhite" : "black",
        "mccs" : [ "string" ],
        "locations" : [ "string" ],
        "channels" : [ "ATM" ]
      } ],
      "declines" : [ {
        "type" : "string",
        "amount" : "string",
        "ignoreRecurring" : true,
        "blackWhite" : "black",
        "mccs" : [ "string" ],
        "locations" : [ "string" ],
        "channels" : [ "ATM" ]
      } ]
    },
    "lastUpdated" : "string"
  } ],
  "moreAvailable" : true,
  "pageNumber" : 0,
  "pageSize" : 0,
  "toDate" : "string",
  "uuid" : "string",
  "notEmpty" : true
}
```


<a name="mdeshistoryusingget"></a>
### Retrieve Paged Transaction History.
```
GET /card/{uuid}/transactions
```


#### Description
Returns all transactions that happened on the provisioned card.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**uuid**  <br>*required*|Card Identifier.|string|
|**Query**|**from**  <br>*required*|from|string|
|**Query**|**pageNumber**  <br>*optional*|pageNumber|integer (int32)|
|**Query**|**pageSize**  <br>*optional*|pageSize|integer (int32)|
|**Query**|**to**  <br>*optional*|to|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[TransactionHistoryDTO](#mdestransactionhistorydto)|
|**400**|Possible causes:<br>Invalid UUID.<br>Invalid value for from.<br>Invalid value for to<br>The value for to is less than the value for from.<br>Invalid page number.<br>Invalid page size.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|Possible causes:<br>No cards provisioned for the UUID.<br>No transactions were found matching specified parameters.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* txn-history


#### Example HTTP request

##### Request path
```
/card/YJtwV4m5gSr25KMV44t_NZx4fQT6lTIWxBjlg0Y5B5I/transactions?from=2018-05-02T00:00:00Z
```


#### Example HTTP response

##### Response 200
```json
{
  "fromDate" : "string",
  "list" : [ {
    "acquirerId" : "123975",
    "cardHolderAmount" : 250.1,
    "channel" : "MOTO",
    "controlsTriggered" : [ "DTA", "ATA" ],
    "emsIssuingCountry" : "US",
    "issuerResponse" : "05",
    "merchantCategoryCode" : "1919",
    "cardAcceptorId" : "123",
    "cardAcceptorName" : "Test Merchant",
    "cardAcceptorCity" : "NY",
    "cardAcceptorStateOrCountry" : "USA",
    "posCountryCode" : "US",
    "preAuthorizedTransaction" : true,
    "recurringTransaction" : true,
    "referenceNumber" : "13948798137",
    "txnAmount" : 250.1,
    "transactionCurrency" : "USD",
    "uuid" : "5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg",
    "trnsmsnDttm" : "string",
    "notifications" : [ {
      "endpointUrl" : "string",
      "timestamp" : "string",
      "httpResponseCode" : "string",
      "banknetRef" : "string",
      "pageNumber" : 0,
      "pageSize" : 0,
      "uuid" : "string"
    } ]
  } ],
  "moreAvailable" : true,
  "pageNumber" : 0,
  "pageSize" : 0,
  "toDate" : "string",
  "uuid" : "string",
  "notEmpty" : true
}
```


<a name="mdescreatebatchusingpost"></a>
### Register a user card.
```
POST /controls/batch
```


#### Description
Creating a batch and assigning a UUID for successful registered cards and also successful completion of batch will return the Batch UUID.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**body**  <br>*required*|List of Card numbers and controls to be applied for that Card numbers.|[CreateBatchDTO](#mdescreatebatchdto)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[BatchIdResponseDTO](#mdesbatchidresponsedto)|
|**400**|Invalid Account Number.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>Account Number not allowed to be provisioned for the registered ICA associated with ClientID.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* batch


#### Example HTTP request

##### Request path
```
/controls/batch
```


##### Request body
```json
{
  "cards" : [ {
    "accountNumber" : "string",
    "recId" : "string"
  } ],
  "controls" : {
    "alerts" : [ {
      "type" : "string",
      "amount" : "string",
      "ignoreRecurring" : true,
      "blackWhite" : "black",
      "mccs" : [ "string" ],
      "locations" : [ "string" ],
      "channels" : [ "ATM" ]
    } ],
    "declines" : [ {
      "type" : "string",
      "amount" : "string",
      "ignoreRecurring" : true,
      "blackWhite" : "black",
      "mccs" : [ "string" ],
      "locations" : [ "string" ],
      "channels" : [ "ATM" ]
    } ]
  }
}
```


#### Example HTTP response

##### Response 200
```json
{
  "batchId" : "lESxCB6fHjzOcetSjhgsiaZgSubLb0t6XiXFoz5Fa9E"
}
```


<a name="mdesretrievebatchusingget"></a>
### Retrieve Batch details
```
GET /controls/batch/{batchId}
```


#### Description
Retrieving the details of the batch ID provided in the url and validate. If it success, last modified date, expiry date and status will be displayed in header section


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**batchId**  <br>*required*|Getting Batchidentifier from URL|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[BatchPayloadResponseDTO](#mdesbatchpayloadresponsedto)|
|**202**|Batch is either queued or inprogress and there is nothing to return.|string|
|**400**|Invalid batchId.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>batchId is provisioned for a different ClientID.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* batch


#### Example HTTP request

##### Request path
```
/controls/batch/lESxCB6fHjzOcetSjhgsiaZgSubLb0t6XiXFoz5Fa9E
```


#### Example HTTP response

##### Response 200
```json
{
  "cards" : [ {
    "uuid" : "5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg",
    "recId" : "string"
  } ],
  "controls" : {
    "alerts" : [ {
      "type" : "string",
      "amount" : "string",
      "ignoreRecurring" : true,
      "blackWhite" : "black",
      "mccs" : [ "string" ],
      "locations" : [ "string" ],
      "channels" : [ "ATM" ]
    } ],
    "declines" : [ {
      "type" : "string",
      "amount" : "string",
      "ignoreRecurring" : true,
      "blackWhite" : "black",
      "mccs" : [ "string" ],
      "locations" : [ "string" ],
      "channels" : [ "ATM" ]
    } ]
  }
}
```


##### Response 202
```json
"string"
```


<a name="mdessendnotificationusingpost"></a>
### Test API to validate the issuer endpoint
```
POST /test/notificationUrl
```


#### Description
This test endpoint allows the Issuer to send a Notification payload which will be passed through to their endpoint as a notification. This endpoint will only work in Sandbox and this is only for testing purposes.


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Header**|**x-request-endpoint-uri**  <br>*required*|The issuer endpoint that will receive the Notification payload|string|
|**Body**|**payload**  <br>*optional*|The notification payload to send to the issuer's endpoint.|[Notification](#mdesnotification)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK  <br>**Headers** :   <br>`x-notification-origin` (boolean)|object|
|**400**|Invalid URI provided in the Request Header.|No Content|
|**401**|Possible causes:<br>ClientID not provisioned.<br>UUID is provisioned for a different ICA than the one associated with ClientID.|No Content|
|**404**|Unable to send notification payload to the Issuer's endpoint.|No Content|


#### Consumes

* `application/json`


#### Produces

* `application/json`


#### Tags

* notification


#### Example HTTP request

##### Request path
```
/test/notificationUrl
```


##### Request header
```json
"http://requestb.in/xt4acwxt"
```


##### Request body
```json
{
  "acquirerId" : "123975",
  "cardholderAmount" : 250.1,
  "channel" : "MOTO",
  "controlsTriggered" : [ "DTA", "ATA" ],
  "emsIssuingCountry" : "US",
  "issuerResponse" : "05",
  "merchantCategoryCode" : "1919",
  "cardAcceptorId" : "123",
  "cardAcceptorName" : "Test Merchant",
  "cardAcceptorCity" : "NY",
  "cardAcceptorStateOrCountry" : "USA",
  "posCountryCode" : "US",
  "preAuthorizedTransaction" : true,
  "recurringTransaction" : true,
  "referenceNumber" : "13948798137",
  "transactionAmount" : 250.1,
  "transactionCurrency" : "USD",
  "uuid" : "5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg",
  "panLastFour" : "string",
  "controlsTriggeredDetails" : "{ATA:amount:300}{DBD:budgetId:XXXXXX,period:MONTHLY,threshold:590.00,spendToDate:600#budgetId:XXXXXX,period:DAILY,threshold:780.00,spendToDate:700}{ADB:budgetId:YYYYYY,period:DAILY,threshold:390.00,spendToDate:300}{AFL:filterId:XXXXX#filterID:YYYYY}"
}
```


#### Example HTTP response

##### Response 200
```json
"object"
```


<a name="mdesdefinitions"></a>
## Definitions

<a name="mdesabstractbudgetsdto"></a>
### AbstractBudgetsDTO

|Name|Description|Schema|
|---|---|---|
|**budgets**  <br>*optional*|**Example** : `[ "[mdesbudgetresponsedto](#mdesbudgetresponsedto)" ]`|< [BudgetResponseDTO](#mdesbudgetresponsedto) > array|
|**type**  <br>*optional*|Required only when posting aggregated controls.  <br>**Example** : `"string"`|string|


<a name="mdesabstractcombinationctrlsdto"></a>
### AbstractCombinationCtrlsDTO

|Name|Description|Schema|
|---|---|---|
|**filterId**  <br>*optional*|**Example** : `"string"`|string|
|**filters**  <br>*optional*|**Example** : `[ "[mdescombinationctrlsresponsedto](#mdescombinationctrlsresponsedto)" ]`|< [CombinationCtrlsResponseDTO](#mdescombinationctrlsresponsedto) > array|
|**lastUpdated**  <br>*optional*|**Example** : `"string"`|string|
|**type**  <br>*optional*|Required only when posting aggregated controls.  <br>**Example** : `"string"`|string|
|**uuid**  <br>*optional*|**Example** : `"string"`|string|


<a name="mdesaddbudgetdto"></a>
### AddBudgetDTO

|Name|Description|Schema|
|---|---|---|
|**filter**  <br>*required*|A filter (controls) to apply the budget to (Please check the filters section).  <br>**Example** : `[ "[mdesfilterdto](#mdesfilterdto)" ]`|< [FilterDTO](#mdesfilterdto) > array|
|**from**  <br>*required*|The start date for the budget control. The date format should be yyyy-MM-dd'T'HH:mm:ssZ  <br>**Example** : `"2019-05-01T19:03:11+0000"`|string|
|**period**  <br>*required*|The budgeting period. One of: daily, hourly, monthly or quarterly.  <br>**Example** : `"daily"`|enum (HOURLY, DAILY, MONTHLY, QUARTERLY)|
|**threshold**  <br>*required*|The Budget Threshold (in cardholder base currency)  <br>**Example** : `"2093001"`|string|


<a name="mdesalertsalldto"></a>
### AlertsAllDTO

|Name|Description|Schema|
|---|---|---|
|**ignoreRecurring**  <br>*optional*|Ignore Recurring Authorizations.  <br>**Example** : `false`|boolean|
|**type**  <br>*optional*|Required only when posting aggregated controls. Refer to sample code above on how to set the request.  <br>**Example** : `"string"`|string|


<a name="mdesalertsdto"></a>
### AlertsDTO

|Name|Description|Schema|
|---|---|---|
|**alerts**  <br>*optional*|**Example** : `[ "[mdescontrolbasedto](#mdescontrolbasedto)" ]`|< [ControlBaseDTO](#mdescontrolbasedto) > array|


<a name="mdesamountalertdto"></a>
### AmountAlertDTO

|Name|Description|Schema|
|---|---|---|
|**amount**  <br>*required*|The amount limit (in cardholder base currency)  <br>**Example** : `"10.0"`|string|
|**type**  <br>*optional*|Required only when posting aggregated controls.  <br>**Example** : `"transactionAmount"`|string|


<a name="mdesamountdeclinedto"></a>
### AmountDeclineDTO

|Name|Description|Schema|
|---|---|---|
|**amount**  <br>*required*|The amount limit (in cardholder base currency)  <br>**Example** : `"10.0"`|string|
|**type**  <br>*optional*|Required only when posting aggregated controls.  <br>**Example** : `"string"`|string|


<a name="mdesbatchidresponsedto"></a>
### BatchIdResponseDTO

|Name|Description|Schema|
|---|---|---|
|**batchId**  <br>*required*|The Batch Identifier for the processed batch of cards  <br>**Example** : `"lESxCB6fHjzOcetSjhgsiaZgSubLb0t6XiXFoz5Fa9E"`|string|


<a name="mdesbatchpandto"></a>
### BatchPanDTO

|Name|Description|Schema|
|---|---|---|
|**accountNumber**  <br>*required*|Card number (Primary Account Number).  <br>**Example** : `"string"`|string|
|**recId**  <br>*required*|Record Id for the Account Number  <br>**Example** : `"string"`|string|


<a name="mdesbatchpanresponsedto"></a>
### BatchPanResponseDTO

|Name|Description|Schema|
|---|---|---|
|**recId**  <br>*required*|Record Id for the Account Number  <br>**Example** : `"string"`|string|
|**uuid**  <br>*required*|The Card Identifier for the registered card.  <br>**Example** : `"5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg"`|string|


<a name="mdesbatchpayloadresponsedto"></a>
### BatchPayloadResponseDTO

|Name|Description|Schema|
|---|---|---|
|**cards**  <br>*optional*|**Example** : `[ "[mdesbatchpanresponsedto](#mdesbatchpanresponsedto)" ]`|< [BatchPanResponseDTO](#mdesbatchpanresponsedto) > array|
|**controls**  <br>*optional*|**Example** : `"[mdescontrolsbatchdto](#mdescontrolsbatchdto)"`|[ControlsBatchDTO](#mdescontrolsbatchdto)|


<a name="mdesbudgetdto"></a>
### BudgetDTO

|Name|Description|Schema|
|---|---|---|
|**budgetId**  <br>*required*|**Example** : `"string"`|string|
|**filter**  <br>*required*|A filter (controls) to apply the budget to (Please check the filters section).  <br>**Example** : `[ "[mdesfilterdto](#mdesfilterdto)" ]`|< [FilterDTO](#mdesfilterdto) > array|
|**from**  <br>*required*|The start date for the budget control. The date format should be yyyy-MM-dd'T'HH:mm:ssZ  <br>**Example** : `"2019-05-01T19:03:11+0000"`|string|
|**period**  <br>*required*|The budgeting period. One of: daily, hourly, monthly or quarterly.  <br>**Example** : `"string"`|enum (HOURLY, DAILY, MONTHLY, QUARTERLY)|
|**threshold**  <br>*required*|The Budget Threshold (in cardholder base currency)  <br>**Example** : `"2093001"`|string|


<a name="mdesbudgetresponsedto"></a>
### BudgetResponseDTO

|Name|Description|Schema|
|---|---|---|
|**budgetId**  <br>*optional*|**Example** : `"string"`|string|
|**filter**  <br>*required*|A filter (controls) to apply the budget to (Please check the filters section).  <br>**Example** : `[ "[mdesfilterdto](#mdesfilterdto)" ]`|< [FilterDTO](#mdesfilterdto) > array|
|**from**  <br>*required*|The start date for the budget control. The date format should be yyyy-MM-dd'T'HH:mm:ssZ  <br>**Example** : `"2019-05-01T19:03:11+0000"`|string|
|**lastUpdated**  <br>*optional*|**Example** : `"2017-05-18T13:40:40+0000"`|string|
|**period**  <br>*required*|The budgeting period. One of: daily, hourly, monthly or quarterly.  <br>**Example** : `"string"`|enum (HOURLY, DAILY, MONTHLY, QUARTERLY)|
|**spendToDate**  <br>*required*|The spend to date  <br>**Example** : `"500"`|string|
|**threshold**  <br>*required*|The Budget Threshold (in cardholder base currency)  <br>**Example** : `"string"`|string|


<a name="mdescarddeclinedto"></a>
### CardDeclineDTO

|Name|Description|Schema|
|---|---|---|
|**ignoreRecurring**  <br>*optional*|Ignore Recurring Authorizations.  <br>**Example** : `false`|boolean|
|**type**  <br>*optional*|Required only when posting aggregated controls. Refer to sample code above on how to set the request.  <br>**Example** : `"string"`|string|


<a name="mdeschannelalertdto"></a>
### ChannelAlertDTO

|Name|Description|Schema|
|---|---|---|
|**channels**  <br>*required*|**Example** : `[ "string" ]`|< string > array|
|**ignoreRecurring**  <br>*optional*|Ignore Recurring Authorizations.  <br>**Example** : `"true"`|string|
|**type**  <br>*optional*|Required only when posting aggregated controls.  <br>**Example** : `"channels"`|string|


<a name="mdeschanneldeclinedto"></a>
### ChannelDeclineDTO

|Name|Description|Schema|
|---|---|---|
|**channels**  <br>*required*|**Example** : `[ "string" ]`|< string > array|
|**ignoreRecurring**  <br>*optional*|Ignore Recurring Authorizations.  <br>**Example** : `"true"`|string|
|**type**  <br>*optional*|Required only when posting aggregated controls.  <br>**Example** : `"channels"`|string|


<a name="mdescombinationctrlsdto"></a>
### CombinationCtrlsDTO

|Name|Description|Schema|
|---|---|---|
|**blackWhite**  <br>*optional*|**Example** : `"string"`|string|
|**filter**  <br>*optional*|**Example** : `[ "[mdescontrolbasedto](#mdescontrolbasedto)" ]`|< [ControlBaseDTO](#mdescontrolbasedto) > array|


<a name="mdescombinationctrlsresponsedto"></a>
### CombinationCtrlsResponseDTO

|Name|Description|Schema|
|---|---|---|
|**blackWhite**  <br>*optional*|**Example** : `"string"`|string|
|**filter**  <br>*optional*|**Example** : `[ "[mdescontrolbasedto](#mdescontrolbasedto)" ]`|< [ControlBaseDTO](#mdescontrolbasedto) > array|
|**filterId**  <br>*optional*|**Example** : `"string"`|string|


<a name="mdescontrolbasedto"></a>
### ControlBaseDTO

|Name|Description|Schema|
|---|---|---|
|**amount**  <br>*optional*|Required only when posting aggregated controls or batch creations. Refer to sample code above on how to set the request.  <br>**Example** : `"string"`|string|
|**blackWhite**  <br>*optional*|specifies whether the rule will be a whitelist or a blacklist  <br>**Example** : `"black"`|string|
|**channels**  <br>*optional*|**Example** : `[ "string" ]`|< string > array|
|**ignoreRecurring**  <br>*optional*|Required only when posting aggregated controls or batch creations. Refer to sample code above on how to set the request.  <br>**Example** : `true`|boolean|
|**locations**  <br>*optional*|**Example** : `[ "string" ]`|< string > array|
|**mccs**  <br>*optional*|**Example** : `[ "string" ]`|< string > array|
|**type**  <br>*optional*|Required only when posting aggregated controls or batch creations. Refer to sample code above on how to set the request.  <br>**Example** : `"string"`|string|


<a name="mdescontrolhistorydto_wrapper"></a>
### ControlHistoryDTO_wrapper
A map containing a list of alert controls and a list of decline controls. Refer to sample code above for more information on how to set the controls.


|Name|Description|Schema|
|---|---|---|
|**controls**  <br>*optional*|**Example** : `"[mdescontrolsdto](#mdescontrolsdto)"`|[ControlsDTO](#mdescontrolsdto)|
|**lastUpdated**  <br>*optional*|**Example** : `"string"`|string (date-time)|


<a name="mdescontrolsbatchdto"></a>
### ControlsBatchDTO

|Name|Description|Schema|
|---|---|---|
|**alerts**  <br>*optional*|**Example** : `[ "[mdescontrolbasedto](#mdescontrolbasedto)" ]`|< [ControlBaseDTO](#mdescontrolbasedto) > array|
|**declines**  <br>*optional*|**Example** : `[ "[mdescontrolbasedto](#mdescontrolbasedto)" ]`|< [ControlBaseDTO](#mdescontrolbasedto) > array|


<a name="mdescontrolsdto"></a>
### ControlsDTO

|Name|Description|Schema|
|---|---|---|
|**alerts**  <br>*optional*|**Example** : `[ "[mdescontrolbasedto](#mdescontrolbasedto)" ]`|< [ControlBaseDTO](#mdescontrolbasedto) > array|
|**declines**  <br>*optional*|**Example** : `[ "[mdescontrolbasedto](#mdescontrolbasedto)" ]`|< [ControlBaseDTO](#mdescontrolbasedto) > array|


<a name="mdescontrolsdto_wrapper"></a>
### ControlsDTO_wrapper
A map containing a list of alert controls and a list of decline controls. Refer to sample code above for more information on how to set the controls.


|Name|Description|Schema|
|---|---|---|
|**controls**  <br>*optional*|**Example** : `"[mdescontrolsdto](#mdescontrolsdto)"`|[ControlsDTO](#mdescontrolsdto)|


<a name="mdescontrolshistorydto"></a>
### ControlsHistoryDTO

|Name|Description|Schema|
|---|---|---|
|**fromDate**  <br>*optional*|**Example** : `"string"`|string (date-time)|
|**list**  <br>*optional*|**Example** : `[ "[mdescontrolhistorydto_wrapper](#mdescontrolhistorydto_wrapper)" ]`|< [ControlHistoryDTO_wrapper](#mdescontrolhistorydto_wrapper) > array|
|**moreAvailable**  <br>*optional*|**Example** : `true`|boolean|
|**notEmpty**  <br>*optional*|**Example** : `true`|boolean|
|**pageNumber**  <br>*optional*|**Example** : `0`|integer (int32)|
|**pageSize**  <br>*optional*|**Example** : `0`|integer (int32)|
|**toDate**  <br>*optional*|**Example** : `"string"`|string (date-time)|
|**uuid**  <br>*optional*|**Example** : `"string"`|string|


<a name="mdescreatebatchdto"></a>
### CreateBatchDTO

|Name|Description|Schema|
|---|---|---|
|**cards**  <br>*optional*|**Example** : `[ "[mdesbatchpandto](#mdesbatchpandto)" ]`|< [BatchPanDTO](#mdesbatchpandto) > array|
|**controls**  <br>*optional*|**Example** : `"[mdescontrolsbatchdto](#mdescontrolsbatchdto)"`|[ControlsBatchDTO](#mdescontrolsbatchdto)|


<a name="mdescrossborderdeclinedto"></a>
### CrossBorderDeclineDTO

|Name|Description|Schema|
|---|---|---|
|**type**  <br>*optional*|Required only when posting aggregated controls.  <br>**Example** : `"string"`|string|


<a name="mdesdeclinesdto"></a>
### DeclinesDTO

|Name|Description|Schema|
|---|---|---|
|**declines**  <br>*optional*|**Example** : `[ "[mdescontrolbasedto](#mdescontrolbasedto)" ]`|< [ControlBaseDTO](#mdescontrolbasedto) > array|


<a name="mdesencodedpandto"></a>
### EncodedPanDTO

|Name|Description|Schema|
|---|---|---|
|**uuid**  <br>*required*|The Card Identifier for the registered card.  <br>**Example** : `"5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg"`|string|


<a name="mdesfilterdto"></a>
### FilterDTO

|Name|Description|Schema|
|---|---|---|
|**type**  <br>*optional*|**Example** : `"All"`|string|


<a name="mdesgeolocationalertdto"></a>
### GeolocationAlertDTO

|Name|Description|Schema|
|---|---|---|
|**blackWhite**  <br>*optional*|specifies whether the rule will be a whitelist or a blacklist  <br>**Example** : `"black"`|string|
|**locations**  <br>*optional*|**Example** : `[ "string" ]`|< string > array|
|**type**  <br>*optional*|Required only when posting aggregated controls.  <br>**Example** : `"string"`|string|


<a name="mdesgeolocationdeclinedto"></a>
### GeolocationDeclineDTO

|Name|Description|Schema|
|---|---|---|
|**blackWhite**  <br>*required*|specifies whether the rule will be a whitelist or a blacklist  <br>**Example** : `"black"`|string|
|**locations**  <br>*optional*|**Example** : `[ "string" ]`|< string > array|
|**type**  <br>*optional*|Required only when posting aggregated controls.  <br>**Example** : `"string"`|string|


<a name="mdesmerchantalertdto"></a>
### MerchantAlertDTO

|Name|Description|Schema|
|---|---|---|
|**blackWhite**  <br>*required*|specifies whether the rule will be a whitelist or a blacklist  <br>**Example** : `"black"`|string|
|**mccs**  <br>*optional*|**Example** : `[ "string" ]`|< string > array|
|**type**  <br>*optional*|Required only when posting aggregated controls.  <br>**Example** : `"string"`|string|


<a name="mdesmerchantdeclinedto"></a>
### MerchantDeclineDTO

|Name|Description|Schema|
|---|---|---|
|**blackWhite**  <br>*optional*|specifies whether the rule will be a whitelist or a blacklist  <br>**Example** : `"black"`|string|
|**mccs**  <br>*optional*|**Example** : `[ "string" ]`|< string > array|
|**type**  <br>*optional*|Required only when posting aggregated controls.  <br>**Example** : `"string"`|string|


<a name="mdesnotification"></a>
### Notification

|Name|Description|Schema|
|---|---|---|
|**acquirerId**  <br>*optional*|Acquiring Institution ID Code.  <br>**Example** : `"123975"`|string|
|**cardAcceptorCity**  <br>*optional*|Card Acceptor City (if available).  <br>**Example** : `"NY"`|string|
|**cardAcceptorId**  <br>*optional*|Card Acceptor ID Code (also known as Merchant ID).  <br>**Example** : `"123"`|string|
|**cardAcceptorName**  <br>*optional*|Card Acceptor Name (also known as the Merchant Doing Business Ad Name).  <br>**Example** : `"Test Merchant"`|string|
|**cardAcceptorStateOrCountry**  <br>*optional*|Card Acceptor State (if in the US) or country if outside the US.  <br>**Example** : `"USA"`|string|
|**cardholderAmount**  <br>*optional*|Cardholder Amount. Number with the decimal points.  <br>**Example** : `250.1`|number (double)|
|**channel**  <br>*optional*|Transaction channel. The channel for transaction - ECOM, MOTO, POS, CONTACTLESS, PWCB, ATM.  <br>**Example** : `"MOTO"`|enum (MOTO, ECOM, ATM, PAYPASS, POS, PWCB)|
|**controlsTriggered**  <br>*optional*|Control(s) triggered. List of the triggered controls. (ATA, ACB, ACH, DCD).  <br>**Example** : `[ "DTA", "ATA" ]`|< string > array|
|**controlsTriggeredDetails**  <br>*optional*|Details of controls triggered. For test notification URL a string is used. For actual notification this will be an array of JSON objects.  <br>**Example** : `"{ATA:amount:300}{DBD:budgetId:XXXXXX,period:MONTHLY,threshold:590.00,spendToDate:600#budgetId:XXXXXX,period:DAILY,threshold:780.00,spendToDate:700}{ADB:budgetId:YYYYYY,period:DAILY,threshold:390.00,spendToDate:300}{AFL:filterId:XXXXX#filterID:YYYYY}"`|string|
|**emsIssuingCountry**  <br>*optional*|EMS Issuing Country. Internal Country Code associated with Card. (3 digit code).  <br>**Example** : `"US"`|string|
|**issuerResponse**  <br>*optional*|Issuer Response. The issuer response code.  <br>**Example** : `"05"`|string|
|**merchantCategoryCode**  <br>*optional*|Merchant Category Code.  <br>**Example** : `"1919"`|string|
|**panLastFour**  <br>*optional*|**Example** : `"string"`|string|
|**posCountryCode**  <br>*optional*|POS Country Code. Country Code associated with the POS. (3 digit code).  <br>**Example** : `"US"`|string|
|**preAuthorizedTransaction**  <br>*optional*|Pre-Authorized transaction Indicator.  <br>**Example** : `true`|boolean|
|**recurringTransaction**  <br>*optional*|Recurring transaction Indicator  <br>**Example** : `true`|boolean|
|**referenceNumber**  <br>*optional*|Reference Number. An identifier for the transaction. Mainly useful for preauthorized and incremental transactions.  <br>**Example** : `"13948798137"`|string|
|**transactionAmount**  <br>*optional*|Transaction Amount. Number with the decimal points.  <br>**Example** : `250.1`|number (double)|
|**transactionCurrency**  <br>*optional*|Transaction Currency. Three digit code indicating the currency.  <br>**Example** : `"USD"`|string|
|**uuid**  <br>*optional*|The Card Identifier for the registered card.  <br>**Example** : `"5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg"`|string|


<a name="mdesnotificationdto"></a>
### NotificationDTO

|Name|Description|Schema|
|---|---|---|
|**banknetRef**  <br>*optional*|**Example** : `"string"`|string|
|**endpointUrl**  <br>*optional*|**Example** : `"string"`|string|
|**httpResponseCode**  <br>*optional*|**Example** : `"string"`|string|
|**pageNumber**  <br>*optional*|**Example** : `0`|integer (int32)|
|**pageSize**  <br>*optional*|**Example** : `0`|integer (int32)|
|**timestamp**  <br>*optional*|**Example** : `"string"`|string (date)|
|**uuid**  <br>*optional*|**Example** : `"string"`|string|


<a name="mdespandto"></a>
### PanDTO

|Name|Description|Schema|
|---|---|---|
|**accountNumber**  <br>*required*|Card number (Primary Account Number).  <br>**Example** : `"5204736610784748"`|string|


<a name="mdestransactiondto"></a>
### TransactionDTO

|Name|Description|Schema|
|---|---|---|
|**acquirerId**  <br>*optional*|Acquiring Institution ID Code.  <br>**Example** : `"123975"`|string|
|**cardAcceptorCity**  <br>*optional*|Card Acceptor City (if available).  <br>**Example** : `"NY"`|string|
|**cardAcceptorId**  <br>*optional*|Card Acceptor ID Code (also known as Merchant ID).  <br>**Example** : `"123"`|string|
|**cardAcceptorName**  <br>*optional*|Card Acceptor Name (also known as the Merchant Doing Business Ad Name).  <br>**Example** : `"Test Merchant"`|string|
|**cardAcceptorStateOrCountry**  <br>*optional*|Card Acceptor State (if in the US) or country if outside the US.  <br>**Example** : `"USA"`|string|
|**cardHolderAmount**  <br>*optional*|Cardholder Amount. Number with the decimal points.  <br>**Example** : `250.1`|number (double)|
|**channel**  <br>*optional*|Transaction channel. The channel for transaction - ECOM, MOTO, POS, CONTACTLESS, PWCB, ATM.  <br>**Example** : `"MOTO"`|enum (MOTO, ECOM, ATM, PAYPASS, POS, PWCB)|
|**controlsTriggered**  <br>*optional*|Control(s) triggered. List of the triggered controls. (ATA, ACB, ACH, DCD).  <br>**Example** : `[ "DTA", "ATA" ]`|< string > array|
|**emsIssuingCountry**  <br>*optional*|EMS Issuing Country. Internal Country Code associated with Card. (3 digit code).  <br>**Example** : `"US"`|string|
|**issuerResponse**  <br>*optional*|Issuer Response. The issuer response code.  <br>**Example** : `"05"`|string|
|**merchantCategoryCode**  <br>*optional*|Merchant Category Code.  <br>**Example** : `"1919"`|string|
|**notifications**  <br>*optional*|**Example** : `[ "[mdesnotificationdto](#mdesnotificationdto)" ]`|< [NotificationDTO](#mdesnotificationdto) > array|
|**posCountryCode**  <br>*optional*|POS Country Code. Country Code associated with the POS. (3 digit code).  <br>**Example** : `"US"`|string|
|**preAuthorizedTransaction**  <br>*optional*|Pre-Authorized transaction Indicator.  <br>**Example** : `true`|boolean|
|**recurringTransaction**  <br>*optional*|Recurring transaction Indicator  <br>**Example** : `true`|boolean|
|**referenceNumber**  <br>*optional*|Reference Number. An identifier for the transaction. Mainly useful for preauthorized and incremental transactions.  <br>**Example** : `"13948798137"`|string|
|**transactionCurrency**  <br>*optional*|Transaction Currency. Three digit code indicating the currency.  <br>**Example** : `"USD"`|string|
|**trnsmsnDttm**  <br>*optional*|**Example** : `"string"`|string (date)|
|**txnAmount**  <br>*optional*|Transaction Amount. Number with the decimal points.  <br>**Example** : `250.1`|number (double)|
|**uuid**  <br>*optional*|The Card Identifier for the registered card.  <br>**Example** : `"5tcTMLIqEg88gN6ClBGqH2TYDQuBDLT4ey5zjQQ7alg"`|string|


<a name="mdestransactionhistorydto"></a>
### TransactionHistoryDTO

|Name|Description|Schema|
|---|---|---|
|**fromDate**  <br>*optional*|**Example** : `"string"`|string (date-time)|
|**list**  <br>*optional*|**Example** : `[ "[mdestransactiondto](#mdestransactiondto)" ]`|< [TransactionDTO](#mdestransactiondto) > array|
|**moreAvailable**  <br>*optional*|**Example** : `true`|boolean|
|**notEmpty**  <br>*optional*|**Example** : `true`|boolean|
|**pageNumber**  <br>*optional*|**Example** : `0`|integer (int32)|
|**pageSize**  <br>*optional*|**Example** : `0`|integer (int32)|
|**toDate**  <br>*optional*|**Example** : `"string"`|string (date-time)|
|**uuid**  <br>*optional*|**Example** : `"string"`|string|


<a name="mdestraveldto"></a>
### TravelDTO

|Name|Description|Schema|
|---|---|---|
|**alias**  <br>*optional*|**Example** : `"Summer Holidays"`|string|
|**blackWhite**  <br>*required*|**Example** : `"black"`|string|
|**locations**  <br>*required*|**Example** : `[ "string" ]`|< string > array|
|**validFrom**  <br>*required*|**Example** : `"2022-03-29T13:08:33+0100"`|string|
|**validTo**  <br>*required*|**Example** : `"2022-05-29T13:08:33+0100"`|string|


<a name="mdestravellistresponsedto"></a>
### TravelListResponseDTO

|Name|Description|Schema|
|---|---|---|
|**travels**  <br>*optional*|**Example** : `[ "[mdestravelresponsedto](#mdestravelresponsedto)" ]`|< [TravelResponseDTO](#mdestravelresponsedto) > array|


<a name="mdestravelresponsedto"></a>
### TravelResponseDTO

|Name|Description|Schema|
|---|---|---|
|**alias**  <br>*optional*|**Example** : `"Summer Holidays"`|string|
|**blackWhite**  <br>*required*|**Example** : `"black"`|string|
|**locations**  <br>*required*|**Example** : `[ "string" ]`|< string > array|
|**travelId**  <br>*required*|**Example** : `"65d59c64-8d12-4bca-a3a2-52f32d35f91d"`|string|
|**validFrom**  <br>*required*|**Example** : `"2022-03-29T13:08:33+0100"`|string|
|**validTo**  <br>*required*|**Example** : `"2022-05-29T13:08:33+0100"`|string|


<a name="mdesbudgetid"></a>
### budgetId

|Name|Description|Schema|
|---|---|---|
|**budgetId**  <br>*required*|Budget Identifier.  <br>**Example** : `"string"`|string|


<a name="mdestravelid"></a>
### travelId

|Name|Description|Schema|
|---|---|---|
|**travelId**  <br>*required*|Travel Identifier.  <br>**Example** : `"string"`|string|







